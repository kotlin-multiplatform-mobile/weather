# Multi Platform Weather | [![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/kotlin-multiplatform-mobile/weather/tree/develop)

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT) [![pipeline status](https://gitlab.com/kotlin-multiplatform-mobile/weather/badges/main/pipeline.svg)](https://gitlab.com/kotlin-multiplatform-mobile/weather/-/commits/main) [![coverage report](https://gitlab.com/kotlin-multiplatform-mobile/weather/badges/main/coverage.svg)](https://weather-kotlin-multiplatform-mobile-56de6775f242a8be9ca34c2ea70.gitlab.io/) [![Latest Release](https://gitlab.com/kotlin-multiplatform-mobile/weather/-/badges/release.svg)](https://gitlab.com/kotlin-multiplatform-mobile/weather/-/releases)

## Contents

- [Documentation](https://gitlab.com/tossaro/kotlin-multi-platform-weather/tree/main/docs)
- [Features](#features)
- [Outputs](#outputs)
- [Requirements](#requirements)
- [Usage](#usage)
- [Project Structure](#project-structure)
- [Architecture](#architecture)
- [Commands](#commands)

## Features

- Provide Weather Utility with Widget Support
- Powered by KOIN for dependency injection and using MVVM pattern with clean architecture.

## Outputs

![Android Day](/resources/android_day.jpeg){width=250px}
![Android Night](/resources/android_night.jpeg){width=250px}

## Requirements

1. Minimum Android/SDK Version: 24
2. Deployment Target iOS/SDK Version: 14.1
3. Target Android/SDK Version: 34
4. Compile Android/SDK Version: 34
5. This project is built using Android Studio version 2023.1.1 and Android Gradle 8.2
6. For iOS, please install [COCOAPODS](https://cocoapods.org/)
7. Update `local.properties` in your root folder, add this content:
```
token={leave_empty}
sonarqubeHost={leave_empty}
sonarqubeToken={leave_empty}
flavors=store,staging
mainFlavor=store
scheme=https
host=example.com
weather_server_store="api.weatherapi.com"
weather_api_key_store="{YOUR_WEATHERAPI_TOKEN}"
```

## Usage

1. Edit settings.gradle in your root folder:

```groovy
dependencyResolutionManagement {
    repositories {
        //...
        maven { url 'https://gitlab.com/api/v4/projects/52245834/packages/maven' }
    }
}
```

2. Last, add 'implementation "multi.platform.weather:weather_shared:${version}"' inside tag
   dependencies { . . . } of build.gradle app

For the high level hierarchy, the project separate into 2 main modules, which are :

### 1. [Weather iOS](https://gitlab.com/kotlin-multiplatform-mobile/weather/tree/main/weather_ios)

This module contains iOS code that holds the iOS library, that can be injected to iOS app.

### 2. [Weather Shared](https://gitlab.com/kotlin-multiplatform-mobile/weather/tree/main/core_shared)

This module contains shared code that holds the domain and data layers and some part of the
presentation logic ie.shared viewmodels.

## Project Structure

```plantuml
:weather_shared;
fork
    :example_android;
fork again
    :weather_ios;
    :example_ios;
end fork
end
```


## Architecture

This project implement
Clean [Architecture by Fernando Cejas](https://github.com/android10/Android-CleanArchitecture)

### Clean architecture

![Image Clean architecture](/resources/clean_architecture.png)

### Architectural approach

![Image Architectural approach](/resources/clean_architecture_layers.png)

### Architectural reactive approach

![Image Architectural reactive approach](/resources/clean_architecture_layers_details.png)

## Commands

Here are some useful gradle/adb commands for executing this example:

* ./gradlew clean build - Build the entire project and execute unit tests
* ./gradlew clean sonarqube - Execute sonarqube coverage report
* ./gradlew assembleStoreDebug dokkaGfm - Generate documentation
* ./gradlew lintStoreDebug - Run linter
* ./gradlew spotlessApply - Run apply spotless
* ./gradlew test{flavor}{buildType}UnitTest - Execute unit tests e.g., testStoreDebugUnitTest
* ./gradlew testStoreDebugUnitTest koverXmlReport - Generate coverage report
* ./gradlew assemble{flavor}{buildType} - Create apk/aar file e.g., assembleStoreDebug
* ./gradlew :weather_shared:assembleXCFramework - Generate XCFramework for iOS
* ./gradlew assembleStoreDebug publish - Publish - Publish to repository packages (MAVEN)