Pod::Spec.new do |spec|
    spec.name                  = 'weather_shared'
    spec.version               = '0.1.3'
    spec.homepage              = 'https://gitlab.com/kotlin-multiplatform-mobile/weather'
    spec.source                = { :git => 'https://gitlab.com/kotlin-multiplatform-mobile/weather.git', :tag => spec.version.to_s }
    spec.license               = { :type => 'MIT', :file => 'LICENSE' }
    spec.summary               = 'Provide Weather Utility with Widget Support'
    spec.weatherors      	   =  { 'Hamzah Tossaro' => 'hamzah.tossaro@gmail.com' }
    spec.summary               = 'Provide base constructor / abstract for simplify code structure'
    spec.vendored_frameworks   = 'weather_shared/build/XCFrameworks/release/weather_shared.xcframework'
    spec.libraries             = 'c++'
    spec.ios.deployment_target = '14.1'
end