#import <Foundation/NSArray.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSError.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSSet.h>
#import <Foundation/NSString.h>
#import <Foundation/NSValue.h>

@class Weather_sharedKoin_coreModule, Weather_sharedWeatherForecastday, Weather_sharedWeather, Weather_sharedWeatherKey, Weather_sharedCore_sharedCoreViewModel, Weather_sharedCore_sharedListViewModel<ITEM, RESP, GET, READ, SET>, Weather_sharedGetWeatherLocationsNetworkUseCase, Weather_sharedGetWeatherLocationsLocalUseCase, Weather_sharedSetWeatherLocationsLocalUseCase, Weather_sharedWeatherLocation, Weather_sharedCore_sharedItemViewModel<ITEM, GET, READ, SET>, Weather_sharedGetWeatherForecastNetworkUseCase, Weather_sharedGetWeatherForecastLocalUseCase, Weather_sharedSetWeatherForecastLocalUseCase, Weather_sharedWeatherAstronomy, Weather_sharedWeatherForecast, Weather_sharedWeatherCompanion, Weather_sharedWeatherCondition, Weather_sharedWeatherAstronomyCompanion, Weather_sharedWeatherConditionCompanion, Weather_sharedWeatherErrorError, Weather_sharedWeatherErrorCompanion, Weather_sharedWeatherError, Weather_sharedWeatherErrorErrorCompanion, Weather_sharedWeatherForecastCompanion, Weather_sharedWeatherForecastDetail, Weather_sharedWeatherForecastDetailCompanion, Weather_sharedWeatherForecastdayCompanion, Weather_sharedWeatherday, Weather_sharedWeatherLocationCompanion, Weather_sharedWeatherdayCompanion, Weather_sharedKotlinArray<T>, Weather_sharedKotlinException, Weather_sharedKoin_coreKoinDefinition<R>, Weather_sharedKoin_coreScope, Weather_sharedKoin_coreParametersHolder, Weather_sharedKoin_coreInstanceFactory<T>, Weather_sharedKoin_coreSingleInstanceFactory<T>, Weather_sharedKoin_coreScopeDSL, Weather_sharedLibrary_baseVersionId, Weather_sharedKotlinByteArray, Weather_sharedLibrary_baseInitialRealmFileConfiguration, Weather_sharedLibrary_baseLogConfiguration, Weather_sharedKotlinx_serialization_coreSerializersModule, Weather_sharedKotlinx_serialization_coreSerialKind, Weather_sharedKotlinNothing, Weather_sharedKotlinThrowable, Weather_sharedKotlinRuntimeException, Weather_sharedKotlinIllegalStateException, NSObject, Weather_sharedKotlinx_serialization_jsonJson, Weather_sharedKtor_httpURLProtocol, Weather_sharedKoin_coreLockable, Weather_sharedKoin_coreKoin, Weather_sharedKotlinLazyThreadSafetyMode, Weather_sharedKoin_coreLogger, Weather_sharedKoin_coreBeanDefinition<T>, Weather_sharedKoin_coreInstanceFactoryCompanion, Weather_sharedKoin_coreInstanceContext, Weather_sharedLibrary_baseUpdatePolicy, Weather_sharedLibrary_baseSort, Weather_sharedKotlinPair<__covariant A, __covariant B>, Weather_sharedKotlinByteIterator, Weather_sharedLibrary_baseLogLevel, Weather_sharedKotlinx_serialization_jsonJsonDefault, Weather_sharedKotlinx_serialization_jsonJsonElement, Weather_sharedKotlinx_serialization_jsonJsonConfiguration, Weather_sharedKtor_httpURLProtocolCompanion, Weather_sharedKoin_coreExtensionManager, Weather_sharedKoin_coreInstanceRegistry, Weather_sharedKoin_corePropertyRegistry, Weather_sharedKoin_coreScopeRegistry, Weather_sharedKotlinEnumCompanion, Weather_sharedKotlinEnum<E>, Weather_sharedKoin_coreLevel, Weather_sharedKoin_coreKind, Weather_sharedKoin_coreCallbacks<T>, Weather_sharedLibrary_baseRealmClassKind, Weather_sharedKotlinx_serialization_jsonJsonElementCompanion, Weather_sharedKoin_coreScopeRegistryCompanion, Weather_sharedLibrary_baseRealmStorageType;

@protocol Weather_sharedWeatherConfig, Weather_sharedLibrary_baseRealm, Weather_sharedLibrary_baseRealmConfiguration, Weather_sharedKotlinx_serialization_coreEncoder, Weather_sharedKotlinx_serialization_coreSerialDescriptor, Weather_sharedKotlinx_serialization_coreSerializationStrategy, Weather_sharedKotlinx_serialization_coreDecoder, Weather_sharedKotlinx_serialization_coreDeserializationStrategy, Weather_sharedKotlinx_serialization_coreKSerializer, Weather_sharedKotlinx_coroutines_coreMutableStateFlow, Weather_sharedKotlinx_coroutines_coreCoroutineScope, Weather_sharedLibrary_baseDeleteable, Weather_sharedLibrary_baseBaseRealmObject, Weather_sharedLibrary_baseTypedRealmObject, Weather_sharedLibrary_baseRealmObject, Weather_sharedCore_sharedCoreUseCase, Weather_sharedWeatherRepository, Weather_sharedCore_sharedApiClientProvider, Weather_sharedKoin_coreQualifier, Weather_sharedKotlinx_coroutines_coreFlow, Weather_sharedLibrary_baseMutableRealm, Weather_sharedLibrary_baseConfiguration, Weather_sharedLibrary_baseRealmQuery, Weather_sharedKotlinKClass, Weather_sharedLibrary_baseRealmSchema, Weather_sharedLibrary_baseVersioned, Weather_sharedLibrary_baseBaseRealm, Weather_sharedLibrary_baseTypedRealm, Weather_sharedLibrary_baseCompactOnLaunchCallback, Weather_sharedLibrary_baseInitialDataCallback, Weather_sharedKotlinx_serialization_coreCompositeEncoder, Weather_sharedKotlinAnnotation, Weather_sharedKotlinx_serialization_coreCompositeDecoder, Weather_sharedKotlinx_coroutines_coreFlowCollector, Weather_sharedKotlinx_coroutines_coreSharedFlow, Weather_sharedKotlinx_coroutines_coreStateFlow, Weather_sharedKotlinx_coroutines_coreMutableSharedFlow, Weather_sharedKotlinCoroutineContext, Weather_sharedKotlinIterator, Weather_sharedCore_sharedCoreConfig, Weather_sharedCore_sharedRefreshTokenUseCase, Weather_sharedKotlinLazy, Weather_sharedKoin_coreScopeCallback, Weather_sharedLibrary_baseRealmScalarQuery, Weather_sharedLibrary_baseRealmSingleQuery, Weather_sharedLibrary_baseRealmScalarNullableQuery, Weather_sharedLibrary_baseRealmElementQuery, Weather_sharedKotlinKDeclarationContainer, Weather_sharedKotlinKAnnotatedElement, Weather_sharedKotlinKClassifier, Weather_sharedLibrary_baseRealmClass, Weather_sharedKotlinComparable, Weather_sharedLibrary_baseRealmLogger, Weather_sharedKotlinx_serialization_coreSerializersModuleCollector, Weather_sharedKotlinCoroutineContextElement, Weather_sharedKotlinCoroutineContextKey, Weather_sharedKotlinx_serialization_coreSerialFormat, Weather_sharedKotlinx_serialization_coreStringFormat, Weather_sharedKoin_coreKoinScopeComponent, Weather_sharedLibrary_baseRealmProperty, Weather_sharedKotlinx_serialization_jsonJsonNamingStrategy, Weather_sharedKoin_coreKoinComponent, Weather_sharedKoin_coreKoinExtension, Weather_sharedLibrary_baseRealmPropertyType;

NS_ASSUME_NONNULL_BEGIN
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-warning-option"
#pragma clang diagnostic ignored "-Wincompatible-property-type"
#pragma clang diagnostic ignored "-Wnullability"

#pragma push_macro("_Nullable_result")
#if !__has_feature(nullability_nullable_result)
#undef _Nullable_result
#define _Nullable_result _Nullable
#endif

__attribute__((swift_name("KotlinBase")))
@interface Weather_sharedBase : NSObject
- (instancetype)init __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
+ (void)initialize __attribute__((objc_requires_super));
@end

@interface Weather_sharedBase (Weather_sharedBaseCopying) <NSCopying>
@end

__attribute__((swift_name("KotlinMutableSet")))
@interface Weather_sharedMutableSet<ObjectType> : NSMutableSet<ObjectType>
@end

__attribute__((swift_name("KotlinMutableDictionary")))
@interface Weather_sharedMutableDictionary<KeyType, ObjectType> : NSMutableDictionary<KeyType, ObjectType>
@end

@interface NSError (NSErrorWeather_sharedKotlinException)
@property (readonly) id _Nullable kotlinException;
@end

__attribute__((swift_name("KotlinNumber")))
@interface Weather_sharedNumber : NSNumber
- (instancetype)initWithChar:(char)value __attribute__((unavailable));
- (instancetype)initWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
- (instancetype)initWithShort:(short)value __attribute__((unavailable));
- (instancetype)initWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
- (instancetype)initWithInt:(int)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
- (instancetype)initWithLong:(long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
- (instancetype)initWithLongLong:(long long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
- (instancetype)initWithFloat:(float)value __attribute__((unavailable));
- (instancetype)initWithDouble:(double)value __attribute__((unavailable));
- (instancetype)initWithBool:(BOOL)value __attribute__((unavailable));
- (instancetype)initWithInteger:(NSInteger)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
+ (instancetype)numberWithChar:(char)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
+ (instancetype)numberWithShort:(short)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
+ (instancetype)numberWithInt:(int)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
+ (instancetype)numberWithLong:(long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
+ (instancetype)numberWithLongLong:(long long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
+ (instancetype)numberWithFloat:(float)value __attribute__((unavailable));
+ (instancetype)numberWithDouble:(double)value __attribute__((unavailable));
+ (instancetype)numberWithBool:(BOOL)value __attribute__((unavailable));
+ (instancetype)numberWithInteger:(NSInteger)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
@end

__attribute__((swift_name("KotlinByte")))
@interface Weather_sharedByte : Weather_sharedNumber
- (instancetype)initWithChar:(char)value;
+ (instancetype)numberWithChar:(char)value;
@end

__attribute__((swift_name("KotlinUByte")))
@interface Weather_sharedUByte : Weather_sharedNumber
- (instancetype)initWithUnsignedChar:(unsigned char)value;
+ (instancetype)numberWithUnsignedChar:(unsigned char)value;
@end

__attribute__((swift_name("KotlinShort")))
@interface Weather_sharedShort : Weather_sharedNumber
- (instancetype)initWithShort:(short)value;
+ (instancetype)numberWithShort:(short)value;
@end

__attribute__((swift_name("KotlinUShort")))
@interface Weather_sharedUShort : Weather_sharedNumber
- (instancetype)initWithUnsignedShort:(unsigned short)value;
+ (instancetype)numberWithUnsignedShort:(unsigned short)value;
@end

__attribute__((swift_name("KotlinInt")))
@interface Weather_sharedInt : Weather_sharedNumber
- (instancetype)initWithInt:(int)value;
+ (instancetype)numberWithInt:(int)value;
@end

__attribute__((swift_name("KotlinUInt")))
@interface Weather_sharedUInt : Weather_sharedNumber
- (instancetype)initWithUnsignedInt:(unsigned int)value;
+ (instancetype)numberWithUnsignedInt:(unsigned int)value;
@end

__attribute__((swift_name("KotlinLong")))
@interface Weather_sharedLong : Weather_sharedNumber
- (instancetype)initWithLongLong:(long long)value;
+ (instancetype)numberWithLongLong:(long long)value;
@end

__attribute__((swift_name("KotlinULong")))
@interface Weather_sharedULong : Weather_sharedNumber
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value;
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value;
@end

__attribute__((swift_name("KotlinFloat")))
@interface Weather_sharedFloat : Weather_sharedNumber
- (instancetype)initWithFloat:(float)value;
+ (instancetype)numberWithFloat:(float)value;
@end

__attribute__((swift_name("KotlinDouble")))
@interface Weather_sharedDouble : Weather_sharedNumber
- (instancetype)initWithDouble:(double)value;
+ (instancetype)numberWithDouble:(double)value;
@end

__attribute__((swift_name("KotlinBoolean")))
@interface Weather_sharedBoolean : Weather_sharedNumber
- (instancetype)initWithBool:(BOOL)value;
+ (instancetype)numberWithBool:(BOOL)value;
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WeatherModule")))
@interface Weather_sharedWeatherModule : Weather_sharedBase
- (instancetype)initWithWeatherConfig:(id<Weather_sharedWeatherConfig>)weatherConfig __attribute__((swift_name("init(weatherConfig:)"))) __attribute__((objc_designated_initializer));
- (Weather_sharedKoin_coreModule *)invoke __attribute__((swift_name("invoke()")));
- (id<Weather_sharedLibrary_baseRealm>)provideDB __attribute__((swift_name("provideDB()")));
@property (readonly) id<Weather_sharedLibrary_baseRealmConfiguration> realmConfig __attribute__((swift_name("realmConfig")));
@end

__attribute__((swift_name("WeatherConfig")))
@protocol Weather_sharedWeatherConfig
@required
@property (readonly) NSString *apiKey __attribute__((swift_name("apiKey")));
@property (readonly) NSString *host __attribute__((swift_name("host")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreSerializationStrategy")))
@protocol Weather_sharedKotlinx_serialization_coreSerializationStrategy
@required
- (void)serializeEncoder:(id<Weather_sharedKotlinx_serialization_coreEncoder>)encoder value:(id _Nullable)value __attribute__((swift_name("serialize(encoder:value:)")));
@property (readonly) id<Weather_sharedKotlinx_serialization_coreSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreDeserializationStrategy")))
@protocol Weather_sharedKotlinx_serialization_coreDeserializationStrategy
@required
- (id _Nullable)deserializeDecoder:(id<Weather_sharedKotlinx_serialization_coreDecoder>)decoder __attribute__((swift_name("deserialize(decoder:)")));
@property (readonly) id<Weather_sharedKotlinx_serialization_coreSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreKSerializer")))
@protocol Weather_sharedKotlinx_serialization_coreKSerializer <Weather_sharedKotlinx_serialization_coreSerializationStrategy, Weather_sharedKotlinx_serialization_coreDeserializationStrategy>
@required
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RealmListWeatherForecastdaySerializer")))
@interface Weather_sharedRealmListWeatherForecastdaySerializer : Weather_sharedBase <Weather_sharedKotlinx_serialization_coreKSerializer>
- (instancetype)initWithDataSerializer:(id<Weather_sharedKotlinx_serialization_coreKSerializer>)dataSerializer __attribute__((swift_name("init(dataSerializer:)"))) __attribute__((objc_designated_initializer));
- (NSMutableArray<Weather_sharedWeatherForecastday *> *)deserializeDecoder:(id<Weather_sharedKotlinx_serialization_coreDecoder>)decoder __attribute__((swift_name("deserialize(decoder:)")));
- (void)serializeEncoder:(id<Weather_sharedKotlinx_serialization_coreEncoder>)encoder value:(NSMutableArray<Weather_sharedWeatherForecastday *> *)value __attribute__((swift_name("serialize(encoder:value:)")));
@property (readonly) id<Weather_sharedKotlinx_serialization_coreSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RealmListWeatherSerializer")))
@interface Weather_sharedRealmListWeatherSerializer : Weather_sharedBase <Weather_sharedKotlinx_serialization_coreKSerializer>
- (instancetype)initWithDataSerializer:(id<Weather_sharedKotlinx_serialization_coreKSerializer>)dataSerializer __attribute__((swift_name("init(dataSerializer:)"))) __attribute__((objc_designated_initializer));
- (NSMutableArray<Weather_sharedWeather *> *)deserializeDecoder:(id<Weather_sharedKotlinx_serialization_coreDecoder>)decoder __attribute__((swift_name("deserialize(decoder:)")));
- (void)serializeEncoder:(id<Weather_sharedKotlinx_serialization_coreEncoder>)encoder value:(NSMutableArray<Weather_sharedWeather *> *)value __attribute__((swift_name("serialize(encoder:value:)")));
@property (readonly) id<Weather_sharedKotlinx_serialization_coreSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WeatherKey")))
@interface Weather_sharedWeatherKey : Weather_sharedBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)weatherKey __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Weather_sharedWeatherKey *shared __attribute__((swift_name("shared")));
@property (readonly) NSString *NIGHT_MODE __attribute__((swift_name("NIGHT_MODE")));
@property (readonly) NSString *QUERY __attribute__((swift_name("QUERY")));
@end

__attribute__((swift_name("Core_sharedCoreViewModel")))
@interface Weather_sharedCore_sharedCoreViewModel : Weather_sharedBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (BOOL)validateBlankField:(id<Weather_sharedKotlinx_coroutines_coreMutableStateFlow>)field error:(id<Weather_sharedKotlinx_coroutines_coreMutableStateFlow>)error __attribute__((swift_name("validateBlank(field:error:)")));
- (BOOL)validateConfirmField1:(id<Weather_sharedKotlinx_coroutines_coreMutableStateFlow>)field1 field2:(id<Weather_sharedKotlinx_coroutines_coreMutableStateFlow>)field2 error:(id<Weather_sharedKotlinx_coroutines_coreMutableStateFlow>)error __attribute__((swift_name("validateConfirm(field1:field2:error:)")));
- (BOOL)validateEmailFormatField:(id<Weather_sharedKotlinx_coroutines_coreMutableStateFlow>)field error:(id<Weather_sharedKotlinx_coroutines_coreMutableStateFlow>)error __attribute__((swift_name("validateEmailFormat(field:error:)")));
- (BOOL)validateMinCharMin:(int32_t)min field:(id<Weather_sharedKotlinx_coroutines_coreMutableStateFlow>)field error:(id<Weather_sharedKotlinx_coroutines_coreMutableStateFlow>)error __attribute__((swift_name("validateMinChar(min:field:error:)")));
- (BOOL)validatePasswordFormatField:(id<Weather_sharedKotlinx_coroutines_coreMutableStateFlow>)field error:(id<Weather_sharedKotlinx_coroutines_coreMutableStateFlow>)error __attribute__((swift_name("validatePasswordFormat(field:error:)")));
- (BOOL)validatePhoneFormatField:(id<Weather_sharedKotlinx_coroutines_coreMutableStateFlow>)field error:(id<Weather_sharedKotlinx_coroutines_coreMutableStateFlow>)error __attribute__((swift_name("validatePhoneFormat(field:error:)")));
@property NSString * _Nullable accessToken __attribute__((swift_name("accessToken")));
@property NSString * _Nullable errorConfirm __attribute__((swift_name("errorConfirm")));
@property NSString * _Nullable errorEmailFormat __attribute__((swift_name("errorEmailFormat")));
@property NSString * _Nullable errorEmptyField __attribute__((swift_name("errorEmptyField")));
@property (readonly) id<Weather_sharedKotlinx_coroutines_coreMutableStateFlow> errorMessage __attribute__((swift_name("errorMessage")));
@property NSString * _Nullable errorMinChar __attribute__((swift_name("errorMinChar")));
@property NSString * _Nullable errorPasswordFormat __attribute__((swift_name("errorPasswordFormat")));
@property NSString * _Nullable errorPhoneFormat __attribute__((swift_name("errorPhoneFormat")));
@property (readonly) id<Weather_sharedKotlinx_coroutines_coreMutableStateFlow> errorResponse __attribute__((swift_name("errorResponse")));
@property (readonly) id<Weather_sharedKotlinx_coroutines_coreMutableStateFlow> forceSignout __attribute__((swift_name("forceSignout")));
@property (readonly) id<Weather_sharedKotlinx_coroutines_coreMutableStateFlow> isEmpty __attribute__((swift_name("isEmpty")));
@property BOOL isFromNetwork __attribute__((swift_name("isFromNetwork")));
@property (readonly) id<Weather_sharedKotlinx_coroutines_coreMutableStateFlow> loadingIndicator __attribute__((swift_name("loadingIndicator")));
@property (readonly) id<Weather_sharedKotlinx_coroutines_coreMutableStateFlow> onException __attribute__((swift_name("onException")));
@property (readonly) id<Weather_sharedKotlinx_coroutines_coreCoroutineScope> scope __attribute__((swift_name("scope")));
@property (readonly) id<Weather_sharedKotlinx_coroutines_coreMutableStateFlow> successMessage __attribute__((swift_name("successMessage")));
@property (readonly) id<Weather_sharedKotlinx_coroutines_coreMutableStateFlow> toastMessage __attribute__((swift_name("toastMessage")));
@property BOOL useAsyncNetworkCall __attribute__((swift_name("useAsyncNetworkCall")));
@end

__attribute__((swift_name("Core_sharedListViewModel")))
@interface Weather_sharedCore_sharedListViewModel<ITEM, RESP, GET, READ, SET> : Weather_sharedCore_sharedCoreViewModel
- (instancetype)initWithGetUseCase:(GET)getUseCase readUseCase:(READ)readUseCase setUseCase:(SET)setUseCase __attribute__((swift_name("init(getUseCase:readUseCase:setUseCase:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (void)getFromLocal __attribute__((swift_name("getFromLocal()")));
- (void)getFromNetwork __attribute__((swift_name("getFromNetwork()")));
- (void)loadIsFromNetworkOpt:(BOOL)isFromNetworkOpt __attribute__((swift_name("load(isFromNetworkOpt:)")));
- (ITEM _Nullable)modifyItemResponseKey:(int32_t)key item:(id _Nullable)item __attribute__((swift_name("modifyItemResponse(key:item:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)saveToLocalNewItems:(NSMutableArray<id> * _Nullable)newItems completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("saveToLocal(newItems:completionHandler:)")));
@property id<Weather_sharedKotlinx_coroutines_coreMutableStateFlow> items __attribute__((swift_name("items")));
@property int32_t limit __attribute__((swift_name("limit")));
@property NSString * _Nullable order __attribute__((swift_name("order")));
@property int32_t page __attribute__((swift_name("page")));
@property id _Nullable search __attribute__((swift_name("search")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ListWeatherLocationViewModel")))
@interface Weather_sharedListWeatherLocationViewModel : Weather_sharedCore_sharedListViewModel<Weather_sharedWeatherLocation *, NSArray<Weather_sharedWeatherLocation *> *, Weather_sharedGetWeatherLocationsNetworkUseCase *, Weather_sharedGetWeatherLocationsLocalUseCase *, Weather_sharedSetWeatherLocationsLocalUseCase *>
- (instancetype)initWithGetUseCase:(Weather_sharedGetWeatherLocationsNetworkUseCase *)getUseCase readUseCase:(Weather_sharedGetWeatherLocationsLocalUseCase *)readUseCase setUseCase:(Weather_sharedSetWeatherLocationsLocalUseCase *)setUseCase __attribute__((swift_name("init(getUseCase:readUseCase:setUseCase:)"))) __attribute__((objc_designated_initializer));
@end

__attribute__((swift_name("Core_sharedItemViewModel")))
@interface Weather_sharedCore_sharedItemViewModel<ITEM, GET, READ, SET> : Weather_sharedCore_sharedCoreViewModel
- (instancetype)initWithGetUseCase:(GET)getUseCase readUseCase:(READ)readUseCase setUseCase:(SET)setUseCase __attribute__((swift_name("init(getUseCase:readUseCase:setUseCase:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (void)getFromLocalIdArg:(id _Nullable)idArg __attribute__((swift_name("getFromLocal(idArg:)")));
- (void)getFromNetworkIdArg:(id _Nullable)idArg __attribute__((swift_name("getFromNetwork(idArg:)")));
- (void)loadIsFromNetworkOpt:(BOOL)isFromNetworkOpt __attribute__((swift_name("load(isFromNetworkOpt:)")));
- (ITEM _Nullable)modifyItemResponseItem:(ITEM _Nullable)item __attribute__((swift_name("modifyItemResponse(item:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)saveToLocalNewItem:(ITEM _Nullable)newItem completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("saveToLocal(newItem:completionHandler:)")));
@property id _Nullable id __attribute__((swift_name("id")));
@property id<Weather_sharedKotlinx_coroutines_coreMutableStateFlow> item __attribute__((swift_name("item")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ItemWeatherForecastViewModel")))
@interface Weather_sharedItemWeatherForecastViewModel : Weather_sharedCore_sharedItemViewModel<Weather_sharedWeatherForecast *, Weather_sharedGetWeatherForecastNetworkUseCase *, Weather_sharedGetWeatherForecastLocalUseCase *, Weather_sharedSetWeatherForecastLocalUseCase *>
- (instancetype)initWithGetUseCase:(Weather_sharedGetWeatherForecastNetworkUseCase *)getUseCase readUseCase:(Weather_sharedGetWeatherForecastLocalUseCase *)readUseCase setUseCase:(Weather_sharedSetWeatherForecastLocalUseCase *)setUseCase __attribute__((swift_name("init(getUseCase:readUseCase:setUseCase:)"))) __attribute__((objc_designated_initializer));
- (NSArray<Weather_sharedWeather *> *)filterWeathersWeatherForecastdays:(NSArray<Weather_sharedWeatherForecastday *> *)weatherForecastdays currentEpoch:(int64_t)currentEpoch __attribute__((swift_name("filterWeathers(weatherForecastdays:currentEpoch:)")));
- (Weather_sharedWeatherAstronomy * _Nullable)prepareAstronomyWeatherForecastdays:(NSArray<Weather_sharedWeatherForecastday *> *)weatherForecastdays currentEpoch:(int64_t)currentEpoch __attribute__((swift_name("prepareAstronomy(weatherForecastdays:currentEpoch:)")));
- (Weather_sharedMutableDictionary<NSString *, id> *)prepareWeathersWeatherForecastdays:(NSArray<Weather_sharedWeatherForecastday *> *)weatherForecastdays currentEpoch:(int64_t)currentEpoch __attribute__((swift_name("prepareWeathers(weatherForecastdays:currentEpoch:)")));
@end

__attribute__((swift_name("WeatherRepository")))
@protocol Weather_sharedWeatherRepository
@required

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)getWeatherForecastLocalSearch:(NSString * _Nullable)search completionHandler:(void (^)(Weather_sharedWeatherForecast * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getWeatherForecastLocal(search:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)getWeatherForecastNetworkQuery:(NSString *)query completionHandler:(void (^)(Weather_sharedWeatherForecast * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getWeatherForecastNetwork(query:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)getWeatherLocationsLocalOffset:(int32_t)offset limit:(int32_t)limit search:(NSString * _Nullable)search completionHandler:(void (^)(NSArray<Weather_sharedWeatherLocation *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getWeatherLocationsLocal(offset:limit:search:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)getWeatherLocationsNetworkOffset:(int32_t)offset limit:(int32_t)limit search:(NSString * _Nullable)search order:(NSString * _Nullable)order completionHandler:(void (^)(NSArray<Weather_sharedWeatherLocation *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getWeatherLocationsNetwork(offset:limit:search:order:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)setWeatherForecastLocalWeatherForecast:(Weather_sharedWeatherForecast *)weatherForecast completionHandler:(void (^)(Weather_sharedWeatherForecast * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("setWeatherForecastLocal(weatherForecast:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)setWeatherLocationsLocalWeatherLocations:(NSArray<Weather_sharedWeatherLocation *> *)weatherLocations completionHandler:(void (^)(Weather_sharedBoolean * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("setWeatherLocationsLocal(weatherLocations:completionHandler:)")));
@end

__attribute__((swift_name("Library_baseDeleteable")))
@protocol Weather_sharedLibrary_baseDeleteable
@required
@end

__attribute__((swift_name("Library_baseBaseRealmObject")))
@protocol Weather_sharedLibrary_baseBaseRealmObject <Weather_sharedLibrary_baseDeleteable>
@required
@end

__attribute__((swift_name("Library_baseTypedRealmObject")))
@protocol Weather_sharedLibrary_baseTypedRealmObject <Weather_sharedLibrary_baseBaseRealmObject>
@required
@end

__attribute__((swift_name("Library_baseRealmObject")))
@protocol Weather_sharedLibrary_baseRealmObject <Weather_sharedLibrary_baseTypedRealmObject>
@required
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
*/
__attribute__((swift_name("Weather")))
@interface Weather_sharedWeather : Weather_sharedBase <Weather_sharedLibrary_baseRealmObject>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) Weather_sharedWeatherCompanion *companion __attribute__((swift_name("companion")));
@property Weather_sharedDouble * _Nullable chanceOfRain __attribute__((swift_name("chanceOfRain")));
@property Weather_sharedInt * _Nullable chanceOfSnow __attribute__((swift_name("chanceOfSnow")));
@property double cloud __attribute__((swift_name("cloud")));
@property Weather_sharedWeatherCondition * _Nullable condition __attribute__((swift_name("condition")));
@property Weather_sharedDouble * _Nullable dewpointC __attribute__((swift_name("dewpointC")));
@property Weather_sharedDouble * _Nullable dewpointF __attribute__((swift_name("dewpointF")));
@property double feelslikeC __attribute__((swift_name("feelslikeC")));
@property double feelslikeF __attribute__((swift_name("feelslikeF")));
@property double gustKph __attribute__((swift_name("gustKph")));
@property double gustMph __attribute__((swift_name("gustMph")));
@property Weather_sharedDouble * _Nullable heatindexC __attribute__((swift_name("heatindexC")));
@property Weather_sharedDouble * _Nullable heatindexF __attribute__((swift_name("heatindexF")));
@property double humidity __attribute__((swift_name("humidity")));
@property Weather_sharedInt * _Nullable isDay __attribute__((swift_name("isDay")));
@property double precipIn __attribute__((swift_name("precipIn")));
@property double precipMm __attribute__((swift_name("precipMm")));
@property double pressureIn __attribute__((swift_name("pressureIn")));
@property double pressureMb __attribute__((swift_name("pressureMb")));
@property double tempC __attribute__((swift_name("tempC")));
@property double tempF __attribute__((swift_name("tempF")));
@property NSString * _Nullable time __attribute__((swift_name("time")));
@property Weather_sharedLong * _Nullable timeEpoch __attribute__((swift_name("timeEpoch")));
@property double uv __attribute__((swift_name("uv")));
@property double visKm __attribute__((swift_name("visKm")));
@property double visMiles __attribute__((swift_name("visMiles")));
@property Weather_sharedInt * _Nullable willItRain __attribute__((swift_name("willItRain")));
@property Weather_sharedInt * _Nullable willItSnow __attribute__((swift_name("willItSnow")));
@property double windDegree __attribute__((swift_name("windDegree")));
@property NSString * _Nullable windDir __attribute__((swift_name("windDir")));
@property double windKph __attribute__((swift_name("windKph")));
@property double windMph __attribute__((swift_name("windMph")));
@property Weather_sharedDouble * _Nullable windchillC __attribute__((swift_name("windchillC")));
@property Weather_sharedDouble * _Nullable windchillF __attribute__((swift_name("windchillF")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Weather.Companion")))
@interface Weather_sharedWeatherCompanion : Weather_sharedBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Weather_sharedWeatherCompanion *shared __attribute__((swift_name("shared")));
- (id)io_realm_kotlin_newInstance __attribute__((swift_name("io_realm_kotlin_newInstance()")));
- (id)io_realm_kotlin_schema __attribute__((swift_name("io_realm_kotlin_schema()")));
- (id<Weather_sharedKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
*/
__attribute__((swift_name("WeatherAstronomy")))
@interface Weather_sharedWeatherAstronomy : Weather_sharedBase <Weather_sharedLibrary_baseRealmObject>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) Weather_sharedWeatherAstronomyCompanion *companion __attribute__((swift_name("companion")));
@property Weather_sharedInt * _Nullable isMoonUp __attribute__((swift_name("isMoonUp")));
@property Weather_sharedInt * _Nullable isSunUp __attribute__((swift_name("isSunUp")));
@property NSString * _Nullable moonIllumination __attribute__((swift_name("moonIllumination")));
@property NSString * _Nullable moonPhase __attribute__((swift_name("moonPhase")));
@property NSString * _Nullable moonrise __attribute__((swift_name("moonrise")));
@property NSString * _Nullable moonset __attribute__((swift_name("moonset")));
@property NSString * _Nullable sunrise __attribute__((swift_name("sunrise")));
@property NSString * _Nullable sunset __attribute__((swift_name("sunset")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WeatherAstronomy.Companion")))
@interface Weather_sharedWeatherAstronomyCompanion : Weather_sharedBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Weather_sharedWeatherAstronomyCompanion *shared __attribute__((swift_name("shared")));
- (id)io_realm_kotlin_newInstance __attribute__((swift_name("io_realm_kotlin_newInstance()")));
- (id)io_realm_kotlin_schema __attribute__((swift_name("io_realm_kotlin_schema()")));
- (id<Weather_sharedKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
*/
__attribute__((swift_name("WeatherCondition")))
@interface Weather_sharedWeatherCondition : Weather_sharedBase <Weather_sharedLibrary_baseRealmObject>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) Weather_sharedWeatherConditionCompanion *companion __attribute__((swift_name("companion")));
@property Weather_sharedInt * _Nullable code __attribute__((swift_name("code")));
@property NSString * _Nullable icon __attribute__((swift_name("icon")));
@property NSString * _Nullable text __attribute__((swift_name("text")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WeatherCondition.Companion")))
@interface Weather_sharedWeatherConditionCompanion : Weather_sharedBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Weather_sharedWeatherConditionCompanion *shared __attribute__((swift_name("shared")));
- (id)io_realm_kotlin_newInstance __attribute__((swift_name("io_realm_kotlin_newInstance()")));
- (id)io_realm_kotlin_schema __attribute__((swift_name("io_realm_kotlin_schema()")));
- (id<Weather_sharedKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WeatherError")))
@interface Weather_sharedWeatherError : Weather_sharedBase
- (instancetype)initWithError:(Weather_sharedWeatherErrorError *)error __attribute__((swift_name("init(error:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Weather_sharedWeatherErrorCompanion *companion __attribute__((swift_name("companion")));
- (Weather_sharedWeatherError *)doCopyError:(Weather_sharedWeatherErrorError *)error __attribute__((swift_name("doCopy(error:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) Weather_sharedWeatherErrorError *error __attribute__((swift_name("error")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WeatherError.Companion")))
@interface Weather_sharedWeatherErrorCompanion : Weather_sharedBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Weather_sharedWeatherErrorCompanion *shared __attribute__((swift_name("shared")));
- (id<Weather_sharedKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WeatherError.Error")))
@interface Weather_sharedWeatherErrorError : Weather_sharedBase
- (instancetype)initWithCode:(NSString * _Nullable)code message:(NSString * _Nullable)message __attribute__((swift_name("init(code:message:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Weather_sharedWeatherErrorErrorCompanion *companion __attribute__((swift_name("companion")));
- (Weather_sharedWeatherErrorError *)doCopyCode:(NSString * _Nullable)code message:(NSString * _Nullable)message __attribute__((swift_name("doCopy(code:message:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable code __attribute__((swift_name("code")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WeatherError.ErrorCompanion")))
@interface Weather_sharedWeatherErrorErrorCompanion : Weather_sharedBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Weather_sharedWeatherErrorErrorCompanion *shared __attribute__((swift_name("shared")));
- (id<Weather_sharedKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
*/
__attribute__((swift_name("WeatherForecast")))
@interface Weather_sharedWeatherForecast : Weather_sharedBase <Weather_sharedLibrary_baseRealmObject>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) Weather_sharedWeatherForecastCompanion *companion __attribute__((swift_name("companion")));
@property Weather_sharedWeather * _Nullable current __attribute__((swift_name("current")));
@property Weather_sharedWeatherForecastDetail * _Nullable forecast __attribute__((swift_name("forecast")));
@property Weather_sharedWeatherLocation * _Nullable location __attribute__((swift_name("location")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WeatherForecast.Companion")))
@interface Weather_sharedWeatherForecastCompanion : Weather_sharedBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Weather_sharedWeatherForecastCompanion *shared __attribute__((swift_name("shared")));
- (id)io_realm_kotlin_newInstance __attribute__((swift_name("io_realm_kotlin_newInstance()")));
- (id)io_realm_kotlin_schema __attribute__((swift_name("io_realm_kotlin_schema()")));
- (id<Weather_sharedKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
*/
__attribute__((swift_name("WeatherForecastDetail")))
@interface Weather_sharedWeatherForecastDetail : Weather_sharedBase <Weather_sharedLibrary_baseRealmObject>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) Weather_sharedWeatherForecastDetailCompanion *companion __attribute__((swift_name("companion")));

/**
 * @note annotations
 *   kotlinx.serialization.Serializable(with=NormalClass(value=multi/platform/weather/shared/external/realm/RealmListWeatherForecastdaySerializer))
*/
@property NSMutableArray<Weather_sharedWeatherForecastday *> * _Nullable forecastday __attribute__((swift_name("forecastday")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WeatherForecastDetail.Companion")))
@interface Weather_sharedWeatherForecastDetailCompanion : Weather_sharedBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Weather_sharedWeatherForecastDetailCompanion *shared __attribute__((swift_name("shared")));
- (id)io_realm_kotlin_newInstance __attribute__((swift_name("io_realm_kotlin_newInstance()")));
- (id)io_realm_kotlin_schema __attribute__((swift_name("io_realm_kotlin_schema()")));
- (id<Weather_sharedKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
*/
__attribute__((swift_name("WeatherForecastday")))
@interface Weather_sharedWeatherForecastday : Weather_sharedBase <Weather_sharedLibrary_baseRealmObject>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) Weather_sharedWeatherForecastdayCompanion *companion __attribute__((swift_name("companion")));
@property Weather_sharedWeatherAstronomy * _Nullable astro __attribute__((swift_name("astro")));
@property NSString * _Nullable date __attribute__((swift_name("date")));
@property Weather_sharedDouble * _Nullable dateEpoch __attribute__((swift_name("dateEpoch")));
@property Weather_sharedWeatherday * _Nullable day __attribute__((swift_name("day")));

/**
 * @note annotations
 *   kotlinx.serialization.Serializable(with=NormalClass(value=multi/platform/weather/shared/external/realm/RealmListWeatherSerializer))
*/
@property NSMutableArray<Weather_sharedWeather *> * _Nullable hour __attribute__((swift_name("hour")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WeatherForecastday.Companion")))
@interface Weather_sharedWeatherForecastdayCompanion : Weather_sharedBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Weather_sharedWeatherForecastdayCompanion *shared __attribute__((swift_name("shared")));
- (id)io_realm_kotlin_newInstance __attribute__((swift_name("io_realm_kotlin_newInstance()")));
- (id)io_realm_kotlin_schema __attribute__((swift_name("io_realm_kotlin_schema()")));
- (id<Weather_sharedKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
*/
__attribute__((swift_name("WeatherLocation")))
@interface Weather_sharedWeatherLocation : Weather_sharedBase <Weather_sharedLibrary_baseRealmObject>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) Weather_sharedWeatherLocationCompanion *companion __attribute__((swift_name("companion")));
@property NSString * _Nullable country __attribute__((swift_name("country")));
@property Weather_sharedInt * _Nullable id __attribute__((swift_name("id")));
@property Weather_sharedDouble * _Nullable lat __attribute__((swift_name("lat")));
@property NSString * _Nullable localtime __attribute__((swift_name("localtime")));
@property Weather_sharedDouble * _Nullable localtimeEpoch __attribute__((swift_name("localtimeEpoch")));
@property Weather_sharedDouble * _Nullable lon __attribute__((swift_name("lon")));
@property NSString * _Nullable name __attribute__((swift_name("name")));
@property NSString * _Nullable region __attribute__((swift_name("region")));
@property NSString * _Nullable tzId __attribute__((swift_name("tzId")));
@property NSString * _Nullable url __attribute__((swift_name("url")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WeatherLocation.Companion")))
@interface Weather_sharedWeatherLocationCompanion : Weather_sharedBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Weather_sharedWeatherLocationCompanion *shared __attribute__((swift_name("shared")));
- (id)io_realm_kotlin_newInstance __attribute__((swift_name("io_realm_kotlin_newInstance()")));
- (id)io_realm_kotlin_schema __attribute__((swift_name("io_realm_kotlin_schema()")));
- (id<Weather_sharedKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
*/
__attribute__((swift_name("Weatherday")))
@interface Weather_sharedWeatherday : Weather_sharedBase <Weather_sharedLibrary_baseRealmObject>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) Weather_sharedWeatherdayCompanion *companion __attribute__((swift_name("companion")));
@property Weather_sharedDouble * _Nullable avghumidity __attribute__((swift_name("avghumidity")));
@property Weather_sharedDouble * _Nullable avgtempC __attribute__((swift_name("avgtempC")));
@property Weather_sharedDouble * _Nullable avgtempF __attribute__((swift_name("avgtempF")));
@property Weather_sharedDouble * _Nullable avgvisKm __attribute__((swift_name("avgvisKm")));
@property Weather_sharedDouble * _Nullable avgvisMiles __attribute__((swift_name("avgvisMiles")));
@property Weather_sharedWeatherCondition * _Nullable condition __attribute__((swift_name("condition")));
@property Weather_sharedDouble * _Nullable dailyChanceOfRain __attribute__((swift_name("dailyChanceOfRain")));
@property Weather_sharedDouble * _Nullable dailyChanceOfSnow __attribute__((swift_name("dailyChanceOfSnow")));
@property Weather_sharedDouble * _Nullable dailyWillItRain __attribute__((swift_name("dailyWillItRain")));
@property Weather_sharedDouble * _Nullable dailyWillItSnow __attribute__((swift_name("dailyWillItSnow")));
@property Weather_sharedDouble * _Nullable maxtempC __attribute__((swift_name("maxtempC")));
@property Weather_sharedDouble * _Nullable maxtempF __attribute__((swift_name("maxtempF")));
@property Weather_sharedDouble * _Nullable maxwindKph __attribute__((swift_name("maxwindKph")));
@property Weather_sharedDouble * _Nullable maxwindMph __attribute__((swift_name("maxwindMph")));
@property Weather_sharedDouble * _Nullable mintempC __attribute__((swift_name("mintempC")));
@property Weather_sharedDouble * _Nullable mintempF __attribute__((swift_name("mintempF")));
@property Weather_sharedDouble * _Nullable totalprecipIn __attribute__((swift_name("totalprecipIn")));
@property Weather_sharedDouble * _Nullable totalprecipMm __attribute__((swift_name("totalprecipMm")));
@property Weather_sharedDouble * _Nullable totalsnowCm __attribute__((swift_name("totalsnowCm")));
@property Weather_sharedDouble * _Nullable uv __attribute__((swift_name("uv")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Weatherday.Companion")))
@interface Weather_sharedWeatherdayCompanion : Weather_sharedBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Weather_sharedWeatherdayCompanion *shared __attribute__((swift_name("shared")));
- (id)io_realm_kotlin_newInstance __attribute__((swift_name("io_realm_kotlin_newInstance()")));
- (id)io_realm_kotlin_schema __attribute__((swift_name("io_realm_kotlin_schema()")));
- (id<Weather_sharedKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end

__attribute__((swift_name("Core_sharedCoreUseCase")))
@protocol Weather_sharedCore_sharedCoreUseCase
@required

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)callArgs:(Weather_sharedKotlinArray<id> *)args completionHandler:(void (^)(id _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("call(args:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)onErrorE:(Weather_sharedKotlinException *)e completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("onError(e:completionHandler:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetWeatherForecastLocalUseCase")))
@interface Weather_sharedGetWeatherForecastLocalUseCase : Weather_sharedBase <Weather_sharedCore_sharedCoreUseCase>
- (instancetype)initWithWeatherRepository:(id<Weather_sharedWeatherRepository>)weatherRepository __attribute__((swift_name("init(weatherRepository:)"))) __attribute__((objc_designated_initializer));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)callArgs:(Weather_sharedKotlinArray<id> *)args completionHandler:(void (^)(Weather_sharedWeatherForecast * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("call(args:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)onErrorE:(Weather_sharedKotlinException *)e completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("onError(e:completionHandler:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetWeatherForecastNetworkUseCase")))
@interface Weather_sharedGetWeatherForecastNetworkUseCase : Weather_sharedBase <Weather_sharedCore_sharedCoreUseCase>
- (instancetype)initWithWeatherRepository:(id<Weather_sharedWeatherRepository>)weatherRepository __attribute__((swift_name("init(weatherRepository:)"))) __attribute__((objc_designated_initializer));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)callArgs:(Weather_sharedKotlinArray<id> *)args completionHandler:(void (^)(Weather_sharedWeatherForecast * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("call(args:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)onErrorE:(Weather_sharedKotlinException *)e completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("onError(e:completionHandler:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetWeatherLocationsLocalUseCase")))
@interface Weather_sharedGetWeatherLocationsLocalUseCase : Weather_sharedBase <Weather_sharedCore_sharedCoreUseCase>
- (instancetype)initWithWeatherRepository:(id<Weather_sharedWeatherRepository>)weatherRepository __attribute__((swift_name("init(weatherRepository:)"))) __attribute__((objc_designated_initializer));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)callArgs:(Weather_sharedKotlinArray<id> *)args completionHandler:(void (^)(NSArray<Weather_sharedWeatherLocation *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("call(args:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)onErrorE:(Weather_sharedKotlinException *)e completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("onError(e:completionHandler:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetWeatherLocationsNetworkUseCase")))
@interface Weather_sharedGetWeatherLocationsNetworkUseCase : Weather_sharedBase <Weather_sharedCore_sharedCoreUseCase>
- (instancetype)initWithWeatherRepository:(id<Weather_sharedWeatherRepository>)weatherRepository __attribute__((swift_name("init(weatherRepository:)"))) __attribute__((objc_designated_initializer));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)callArgs:(Weather_sharedKotlinArray<id> *)args completionHandler:(void (^)(NSArray<Weather_sharedWeatherLocation *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("call(args:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)onErrorE:(Weather_sharedKotlinException *)e completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("onError(e:completionHandler:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SetWeatherForecastLocalUseCase")))
@interface Weather_sharedSetWeatherForecastLocalUseCase : Weather_sharedBase <Weather_sharedCore_sharedCoreUseCase>
- (instancetype)initWithWeatherRepository:(id<Weather_sharedWeatherRepository>)weatherRepository __attribute__((swift_name("init(weatherRepository:)"))) __attribute__((objc_designated_initializer));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)callArgs:(Weather_sharedKotlinArray<id> *)args completionHandler:(void (^)(Weather_sharedWeatherForecast * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("call(args:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)onErrorE:(Weather_sharedKotlinException *)e completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("onError(e:completionHandler:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SetWeatherLocationsLocalUseCase")))
@interface Weather_sharedSetWeatherLocationsLocalUseCase : Weather_sharedBase <Weather_sharedCore_sharedCoreUseCase>
- (instancetype)initWithWeatherRepository:(id<Weather_sharedWeatherRepository>)weatherRepository __attribute__((swift_name("init(weatherRepository:)"))) __attribute__((objc_designated_initializer));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)callArgs:(Weather_sharedKotlinArray<id> *)args completionHandler:(void (^)(Weather_sharedBoolean * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("call(args:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)onErrorE:(Weather_sharedKotlinException *)e completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("onError(e:completionHandler:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WeatherRepositoryImpl")))
@interface Weather_sharedWeatherRepositoryImpl : Weather_sharedBase <Weather_sharedWeatherRepository>
- (instancetype)initWithRealm:(id<Weather_sharedLibrary_baseRealm>)realm weatherConfig:(id<Weather_sharedWeatherConfig>)weatherConfig apiClientProvider:(id<Weather_sharedCore_sharedApiClientProvider>)apiClientProvider __attribute__((swift_name("init(realm:weatherConfig:apiClientProvider:)"))) __attribute__((objc_designated_initializer));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)getWeatherForecastLocalSearch:(NSString * _Nullable)search completionHandler:(void (^)(Weather_sharedWeatherForecast * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getWeatherForecastLocal(search:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)getWeatherForecastNetworkQuery:(NSString *)query completionHandler:(void (^)(Weather_sharedWeatherForecast * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getWeatherForecastNetwork(query:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)getWeatherLocationsLocalOffset:(int32_t)offset limit:(int32_t)limit search:(NSString * _Nullable)search completionHandler:(void (^)(NSArray<Weather_sharedWeatherLocation *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getWeatherLocationsLocal(offset:limit:search:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)getWeatherLocationsNetworkOffset:(int32_t)offset limit:(int32_t)limit search:(NSString * _Nullable)search order:(NSString * _Nullable)order completionHandler:(void (^)(NSArray<Weather_sharedWeatherLocation *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getWeatherLocationsNetwork(offset:limit:search:order:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)setWeatherForecastLocalWeatherForecast:(Weather_sharedWeatherForecast *)weatherForecast completionHandler:(void (^)(Weather_sharedWeatherForecast * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("setWeatherForecastLocal(weatherForecast:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)setWeatherLocationsLocalWeatherLocations:(NSArray<Weather_sharedWeatherLocation *> *)weatherLocations completionHandler:(void (^)(Weather_sharedBoolean * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("setWeatherLocationsLocal(weatherLocations:completionHandler:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ActualKt")))
@interface Weather_sharedActualKt : Weather_sharedBase
+ (Weather_sharedKoin_coreModule *)weatherModule __attribute__((swift_name("weatherModule()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_coreModule")))
@interface Weather_sharedKoin_coreModule : Weather_sharedBase
- (instancetype)initWith_createdAtStart:(BOOL)_createdAtStart __attribute__((swift_name("init(_createdAtStart:)"))) __attribute__((objc_designated_initializer));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (Weather_sharedKoin_coreKoinDefinition<id> *)factoryQualifier:(id<Weather_sharedKoin_coreQualifier> _Nullable)qualifier definition:(id _Nullable (^)(Weather_sharedKoin_coreScope *, Weather_sharedKoin_coreParametersHolder *))definition __attribute__((swift_name("factory(qualifier:definition:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (void)includesModule:(Weather_sharedKotlinArray<Weather_sharedKoin_coreModule *> *)module __attribute__((swift_name("includes(module:)")));
- (void)includesModule_:(NSArray<Weather_sharedKoin_coreModule *> *)module __attribute__((swift_name("includes(module_:)")));
- (void)indexPrimaryTypeInstanceFactory:(Weather_sharedKoin_coreInstanceFactory<id> *)instanceFactory __attribute__((swift_name("indexPrimaryType(instanceFactory:)")));
- (void)indexSecondaryTypesInstanceFactory:(Weather_sharedKoin_coreInstanceFactory<id> *)instanceFactory __attribute__((swift_name("indexSecondaryTypes(instanceFactory:)")));
- (NSArray<Weather_sharedKoin_coreModule *> *)plusModules:(NSArray<Weather_sharedKoin_coreModule *> *)modules __attribute__((swift_name("plus(modules:)")));
- (NSArray<Weather_sharedKoin_coreModule *> *)plusModule:(Weather_sharedKoin_coreModule *)module __attribute__((swift_name("plus(module:)")));
- (void)prepareForCreationAtStartInstanceFactory:(Weather_sharedKoin_coreSingleInstanceFactory<id> *)instanceFactory __attribute__((swift_name("prepareForCreationAtStart(instanceFactory:)")));
- (void)scopeQualifier:(id<Weather_sharedKoin_coreQualifier>)qualifier scopeSet:(void (^)(Weather_sharedKoin_coreScopeDSL *))scopeSet __attribute__((swift_name("scope(qualifier:scopeSet:)")));
- (void)scopeScopeSet:(void (^)(Weather_sharedKoin_coreScopeDSL *))scopeSet __attribute__((swift_name("scope(scopeSet:)")));
- (Weather_sharedKoin_coreKoinDefinition<id> *)singleQualifier:(id<Weather_sharedKoin_coreQualifier> _Nullable)qualifier createdAtStart:(BOOL)createdAtStart definition:(id _Nullable (^)(Weather_sharedKoin_coreScope *, Weather_sharedKoin_coreParametersHolder *))definition __attribute__((swift_name("single(qualifier:createdAtStart:definition:)")));
@property (readonly) Weather_sharedMutableSet<Weather_sharedKoin_coreSingleInstanceFactory<id> *> *eagerInstances __attribute__((swift_name("eagerInstances")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) NSMutableArray<Weather_sharedKoin_coreModule *> *includedModules __attribute__((swift_name("includedModules")));
@property (readonly) BOOL isLoaded __attribute__((swift_name("isLoaded")));
@property (readonly) Weather_sharedMutableDictionary<NSString *, Weather_sharedKoin_coreInstanceFactory<id> *> *mappings __attribute__((swift_name("mappings")));
@end

__attribute__((swift_name("Library_baseVersioned")))
@protocol Weather_sharedLibrary_baseVersioned
@required
- (Weather_sharedLibrary_baseVersionId *)version __attribute__((swift_name("version()")));
@end

__attribute__((swift_name("Library_baseBaseRealm")))
@protocol Weather_sharedLibrary_baseBaseRealm <Weather_sharedLibrary_baseVersioned>
@required
- (int64_t)getNumberOfActiveVersions __attribute__((swift_name("getNumberOfActiveVersions()")));
- (BOOL)isClosed __attribute__((swift_name("isClosed()")));
- (id<Weather_sharedLibrary_baseRealmSchema>)schema __attribute__((swift_name("schema()")));
- (int64_t)schemaVersion __attribute__((swift_name("schemaVersion()")));
@property (readonly) id<Weather_sharedLibrary_baseConfiguration> configuration __attribute__((swift_name("configuration")));
@end

__attribute__((swift_name("Library_baseTypedRealm")))
@protocol Weather_sharedLibrary_baseTypedRealm <Weather_sharedLibrary_baseBaseRealm>
@required
- (id<Weather_sharedLibrary_baseTypedRealmObject>)doCopyFromRealmObj:(id<Weather_sharedLibrary_baseTypedRealmObject>)obj depth:(uint32_t)depth __attribute__((swift_name("doCopyFromRealm(obj:depth:)")));
- (NSDictionary<NSString *, id> *)doCopyFromRealmDictionary:(Weather_sharedMutableDictionary<NSString *, id> *)dictionary depth:(uint32_t)depth __attribute__((swift_name("doCopyFromRealm(dictionary:depth:)")));
- (NSArray<id<Weather_sharedLibrary_baseTypedRealmObject>> *)doCopyFromRealmCollection:(id)collection depth:(uint32_t)depth __attribute__((swift_name("doCopyFromRealm(collection:depth:)")));
- (id<Weather_sharedLibrary_baseRealmQuery>)queryClazz:(id<Weather_sharedKotlinKClass>)clazz query:(NSString *)query args:(Weather_sharedKotlinArray<id> *)args __attribute__((swift_name("query(clazz:query:args:)")));
@end

__attribute__((swift_name("Library_baseRealm")))
@protocol Weather_sharedLibrary_baseRealm <Weather_sharedLibrary_baseTypedRealm>
@required
- (id<Weather_sharedKotlinx_coroutines_coreFlow>)asFlow __attribute__((swift_name("asFlow()")));
- (void)close __attribute__((swift_name("close()")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)writeBlock:(id _Nullable (^)(id<Weather_sharedLibrary_baseMutableRealm>))block completionHandler:(void (^)(id _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("write(block:completionHandler:)")));
- (id _Nullable)writeBlockingBlock:(id _Nullable (^)(id<Weather_sharedLibrary_baseMutableRealm>))block __attribute__((swift_name("writeBlocking(block:)")));
- (void)writeCopyToTargetConfiguration:(id<Weather_sharedLibrary_baseConfiguration>)targetConfiguration __attribute__((swift_name("writeCopyTo(targetConfiguration:)")));
@end

__attribute__((swift_name("Library_baseConfiguration")))
@protocol Weather_sharedLibrary_baseConfiguration
@required
@property (readonly) id<Weather_sharedLibrary_baseCompactOnLaunchCallback> _Nullable compactOnLaunchCallback __attribute__((swift_name("compactOnLaunchCallback")));
@property (readonly) Weather_sharedKotlinByteArray * _Nullable encryptionKey __attribute__((swift_name("encryptionKey")));
@property (readonly) BOOL inMemory __attribute__((swift_name("inMemory")));
@property (readonly) id<Weather_sharedLibrary_baseInitialDataCallback> _Nullable initialDataCallback __attribute__((swift_name("initialDataCallback")));
@property (readonly) Weather_sharedLibrary_baseInitialRealmFileConfiguration * _Nullable initialRealmFileConfiguration __attribute__((swift_name("initialRealmFileConfiguration")));
@property (readonly) Weather_sharedLibrary_baseLogConfiguration *log __attribute__((swift_name("log"))) __attribute__((deprecated("Use io.realm.kotlin.log.RealmLog instead.")));
@property (readonly) int64_t maxNumberOfActiveVersions __attribute__((swift_name("maxNumberOfActiveVersions")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) NSString *path __attribute__((swift_name("path")));
@property (readonly, getter=schema_) NSSet<id<Weather_sharedKotlinKClass>> *schema __attribute__((swift_name("schema")));
@property (readonly, getter=schemaVersion_) int64_t schemaVersion __attribute__((swift_name("schemaVersion")));
@end

__attribute__((swift_name("Library_baseRealmConfiguration")))
@protocol Weather_sharedLibrary_baseRealmConfiguration <Weather_sharedLibrary_baseConfiguration>
@required
@property (readonly) BOOL deleteRealmIfMigrationNeeded __attribute__((swift_name("deleteRealmIfMigrationNeeded")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreEncoder")))
@protocol Weather_sharedKotlinx_serialization_coreEncoder
@required
- (id<Weather_sharedKotlinx_serialization_coreCompositeEncoder>)beginCollectionDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor collectionSize:(int32_t)collectionSize __attribute__((swift_name("beginCollection(descriptor:collectionSize:)")));
- (id<Weather_sharedKotlinx_serialization_coreCompositeEncoder>)beginStructureDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("beginStructure(descriptor:)")));
- (void)encodeBooleanValue:(BOOL)value __attribute__((swift_name("encodeBoolean(value:)")));
- (void)encodeByteValue:(int8_t)value __attribute__((swift_name("encodeByte(value:)")));
- (void)encodeCharValue:(unichar)value __attribute__((swift_name("encodeChar(value:)")));
- (void)encodeDoubleValue:(double)value __attribute__((swift_name("encodeDouble(value:)")));
- (void)encodeEnumEnumDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)enumDescriptor index:(int32_t)index __attribute__((swift_name("encodeEnum(enumDescriptor:index:)")));
- (void)encodeFloatValue:(float)value __attribute__((swift_name("encodeFloat(value:)")));
- (id<Weather_sharedKotlinx_serialization_coreEncoder>)encodeInlineDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("encodeInline(descriptor:)")));
- (void)encodeIntValue:(int32_t)value __attribute__((swift_name("encodeInt(value:)")));
- (void)encodeLongValue:(int64_t)value __attribute__((swift_name("encodeLong(value:)")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (void)encodeNotNullMark __attribute__((swift_name("encodeNotNullMark()")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (void)encodeNull __attribute__((swift_name("encodeNull()")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (void)encodeNullableSerializableValueSerializer:(id<Weather_sharedKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeNullableSerializableValue(serializer:value:)")));
- (void)encodeSerializableValueSerializer:(id<Weather_sharedKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeSerializableValue(serializer:value:)")));
- (void)encodeShortValue:(int16_t)value __attribute__((swift_name("encodeShort(value:)")));
- (void)encodeStringValue:(NSString *)value __attribute__((swift_name("encodeString(value:)")));
@property (readonly) Weather_sharedKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreSerialDescriptor")))
@protocol Weather_sharedKotlinx_serialization_coreSerialDescriptor
@required

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (NSArray<id<Weather_sharedKotlinAnnotation>> *)getElementAnnotationsIndex:(int32_t)index __attribute__((swift_name("getElementAnnotations(index:)")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)getElementDescriptorIndex:(int32_t)index __attribute__((swift_name("getElementDescriptor(index:)")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (int32_t)getElementIndexName:(NSString *)name __attribute__((swift_name("getElementIndex(name:)")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (NSString *)getElementNameIndex:(int32_t)index __attribute__((swift_name("getElementName(index:)")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (BOOL)isElementOptionalIndex:(int32_t)index __attribute__((swift_name("isElementOptional(index:)")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
@property (readonly) NSArray<id<Weather_sharedKotlinAnnotation>> *annotations __attribute__((swift_name("annotations")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
@property (readonly) int32_t elementsCount __attribute__((swift_name("elementsCount")));
@property (readonly) BOOL isInline __attribute__((swift_name("isInline")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
@property (readonly) BOOL isNullable __attribute__((swift_name("isNullable")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
@property (readonly) Weather_sharedKotlinx_serialization_coreSerialKind *kind __attribute__((swift_name("kind")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
@property (readonly) NSString *serialName __attribute__((swift_name("serialName")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreDecoder")))
@protocol Weather_sharedKotlinx_serialization_coreDecoder
@required
- (id<Weather_sharedKotlinx_serialization_coreCompositeDecoder>)beginStructureDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("beginStructure(descriptor:)")));
- (BOOL)decodeBoolean __attribute__((swift_name("decodeBoolean()")));
- (int8_t)decodeByte __attribute__((swift_name("decodeByte()")));
- (unichar)decodeChar __attribute__((swift_name("decodeChar()")));
- (double)decodeDouble __attribute__((swift_name("decodeDouble()")));
- (int32_t)decodeEnumEnumDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)enumDescriptor __attribute__((swift_name("decodeEnum(enumDescriptor:)")));
- (float)decodeFloat __attribute__((swift_name("decodeFloat()")));
- (id<Weather_sharedKotlinx_serialization_coreDecoder>)decodeInlineDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("decodeInline(descriptor:)")));
- (int32_t)decodeInt __attribute__((swift_name("decodeInt()")));
- (int64_t)decodeLong __attribute__((swift_name("decodeLong()")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (BOOL)decodeNotNullMark __attribute__((swift_name("decodeNotNullMark()")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (Weather_sharedKotlinNothing * _Nullable)decodeNull __attribute__((swift_name("decodeNull()")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (id _Nullable)decodeNullableSerializableValueDeserializer:(id<Weather_sharedKotlinx_serialization_coreDeserializationStrategy>)deserializer __attribute__((swift_name("decodeNullableSerializableValue(deserializer:)")));
- (id _Nullable)decodeSerializableValueDeserializer:(id<Weather_sharedKotlinx_serialization_coreDeserializationStrategy>)deserializer __attribute__((swift_name("decodeSerializableValue(deserializer:)")));
- (int16_t)decodeShort __attribute__((swift_name("decodeShort()")));
- (NSString *)decodeString __attribute__((swift_name("decodeString()")));
@property (readonly) Weather_sharedKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end

__attribute__((swift_name("Kotlinx_coroutines_coreFlow")))
@protocol Weather_sharedKotlinx_coroutines_coreFlow
@required

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)collectCollector:(id<Weather_sharedKotlinx_coroutines_coreFlowCollector>)collector completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("collect(collector:completionHandler:)")));
@end

__attribute__((swift_name("Kotlinx_coroutines_coreSharedFlow")))
@protocol Weather_sharedKotlinx_coroutines_coreSharedFlow <Weather_sharedKotlinx_coroutines_coreFlow>
@required
@property (readonly) NSArray<id> *replayCache __attribute__((swift_name("replayCache")));
@end

__attribute__((swift_name("Kotlinx_coroutines_coreStateFlow")))
@protocol Weather_sharedKotlinx_coroutines_coreStateFlow <Weather_sharedKotlinx_coroutines_coreSharedFlow>
@required
@property (readonly) id _Nullable value __attribute__((swift_name("value")));
@end

__attribute__((swift_name("Kotlinx_coroutines_coreFlowCollector")))
@protocol Weather_sharedKotlinx_coroutines_coreFlowCollector
@required

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)emitValue:(id _Nullable)value completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("emit(value:completionHandler:)")));
@end

__attribute__((swift_name("Kotlinx_coroutines_coreMutableSharedFlow")))
@protocol Weather_sharedKotlinx_coroutines_coreMutableSharedFlow <Weather_sharedKotlinx_coroutines_coreSharedFlow, Weather_sharedKotlinx_coroutines_coreFlowCollector>
@required

/**
 * @note annotations
 *   kotlinx.coroutines.ExperimentalCoroutinesApi
*/
- (void)resetReplayCache __attribute__((swift_name("resetReplayCache()")));
- (BOOL)tryEmitValue:(id _Nullable)value __attribute__((swift_name("tryEmit(value:)")));
@property (readonly) id<Weather_sharedKotlinx_coroutines_coreStateFlow> subscriptionCount __attribute__((swift_name("subscriptionCount")));
@end

__attribute__((swift_name("Kotlinx_coroutines_coreMutableStateFlow")))
@protocol Weather_sharedKotlinx_coroutines_coreMutableStateFlow <Weather_sharedKotlinx_coroutines_coreStateFlow, Weather_sharedKotlinx_coroutines_coreMutableSharedFlow>
@required
- (BOOL)compareAndSetExpect:(id _Nullable)expect update:(id _Nullable)update __attribute__((swift_name("compareAndSet(expect:update:)")));
- (void)setValue:(id _Nullable)value __attribute__((swift_name("setValue(_:)")));
@end

__attribute__((swift_name("Kotlinx_coroutines_coreCoroutineScope")))
@protocol Weather_sharedKotlinx_coroutines_coreCoroutineScope
@required
@property (readonly) id<Weather_sharedKotlinCoroutineContext> coroutineContext __attribute__((swift_name("coroutineContext")));
@end

__attribute__((swift_name("KotlinThrowable")))
@interface Weather_sharedKotlinThrowable : Weather_sharedBase
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(Weather_sharedKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(Weather_sharedKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));

/**
 * @note annotations
 *   kotlin.experimental.ExperimentalNativeApi
*/
- (Weather_sharedKotlinArray<NSString *> *)getStackTrace __attribute__((swift_name("getStackTrace()")));
- (void)printStackTrace __attribute__((swift_name("printStackTrace()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) Weather_sharedKotlinThrowable * _Nullable cause __attribute__((swift_name("cause")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
- (NSError *)asError __attribute__((swift_name("asError()")));
@end

__attribute__((swift_name("KotlinException")))
@interface Weather_sharedKotlinException : Weather_sharedKotlinThrowable
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(Weather_sharedKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(Weather_sharedKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end

__attribute__((swift_name("KotlinRuntimeException")))
@interface Weather_sharedKotlinRuntimeException : Weather_sharedKotlinException
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(Weather_sharedKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(Weather_sharedKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end

__attribute__((swift_name("KotlinIllegalStateException")))
@interface Weather_sharedKotlinIllegalStateException : Weather_sharedKotlinRuntimeException
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(Weather_sharedKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(Weather_sharedKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end


/**
 * @note annotations
 *   kotlin.SinceKotlin(version="1.4")
*/
__attribute__((swift_name("KotlinCancellationException")))
@interface Weather_sharedKotlinCancellationException : Weather_sharedKotlinIllegalStateException
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(Weather_sharedKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(Weather_sharedKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinArray")))
@interface Weather_sharedKotlinArray<T> : Weather_sharedBase
+ (instancetype)arrayWithSize:(int32_t)size init:(T _Nullable (^)(Weather_sharedInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (T _Nullable)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (id<Weather_sharedKotlinIterator>)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(T _Nullable)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end

__attribute__((swift_name("Core_sharedApiClientProvider")))
@protocol Weather_sharedCore_sharedApiClientProvider
@required
@property (readonly) id _Nullable client __attribute__((swift_name("client")));
@property (readonly) NSObject * _Nullable context __attribute__((swift_name("context")));
@property (readonly) id<Weather_sharedCore_sharedCoreConfig> coreConfig __attribute__((swift_name("coreConfig")));
@property (readonly) NSString *deviceId __attribute__((swift_name("deviceId")));
@property (readonly) Weather_sharedKotlinx_serialization_jsonJson *json __attribute__((swift_name("json")));
@property (readonly) id<Weather_sharedCore_sharedRefreshTokenUseCase> _Nullable refreshTokenUseCase __attribute__((swift_name("refreshTokenUseCase")));
@property (readonly) NSString *server __attribute__((swift_name("server")));
@property (readonly) Weather_sharedKtor_httpURLProtocol * _Nullable serverProtocol __attribute__((swift_name("serverProtocol")));
@property (readonly) NSString *sharedPrefsKey __attribute__((swift_name("sharedPrefsKey")));
@property (readonly, getter=version_) NSString *version __attribute__((swift_name("version")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_coreKoinDefinition")))
@interface Weather_sharedKoin_coreKoinDefinition<R> : Weather_sharedBase
- (instancetype)initWithModule:(Weather_sharedKoin_coreModule *)module factory:(Weather_sharedKoin_coreInstanceFactory<R> *)factory __attribute__((swift_name("init(module:factory:)"))) __attribute__((objc_designated_initializer));
- (Weather_sharedKoin_coreKoinDefinition<R> *)doCopyModule:(Weather_sharedKoin_coreModule *)module factory:(Weather_sharedKoin_coreInstanceFactory<R> *)factory __attribute__((swift_name("doCopy(module:factory:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) Weather_sharedKoin_coreInstanceFactory<R> *factory __attribute__((swift_name("factory")));
@property (readonly) Weather_sharedKoin_coreModule *module __attribute__((swift_name("module")));
@end

__attribute__((swift_name("Koin_coreQualifier")))
@protocol Weather_sharedKoin_coreQualifier
@required
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end

__attribute__((swift_name("Koin_coreLockable")))
@interface Weather_sharedKoin_coreLockable : Weather_sharedBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_coreScope")))
@interface Weather_sharedKoin_coreScope : Weather_sharedKoin_coreLockable
- (instancetype)initWithScopeQualifier:(id<Weather_sharedKoin_coreQualifier>)scopeQualifier id:(NSString *)id isRoot:(BOOL)isRoot _koin:(Weather_sharedKoin_coreKoin *)_koin __attribute__((swift_name("init(scopeQualifier:id:isRoot:_koin:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (void)close __attribute__((swift_name("close()")));
- (Weather_sharedKoin_coreScope *)doCopyScopeQualifier:(id<Weather_sharedKoin_coreQualifier>)scopeQualifier id:(NSString *)id isRoot:(BOOL)isRoot _koin:(Weather_sharedKoin_coreKoin *)_koin __attribute__((swift_name("doCopy(scopeQualifier:id:isRoot:_koin:)")));
- (void)declareInstance:(id _Nullable)instance qualifier:(id<Weather_sharedKoin_coreQualifier> _Nullable)qualifier secondaryTypes:(NSArray<id<Weather_sharedKotlinKClass>> *)secondaryTypes allowOverride:(BOOL)allowOverride __attribute__((swift_name("declare(instance:qualifier:secondaryTypes:allowOverride:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (id _Nullable)getClazz:(id<Weather_sharedKotlinKClass>)clazz qualifier:(id<Weather_sharedKoin_coreQualifier> _Nullable)qualifier parameters:(Weather_sharedKoin_coreParametersHolder *(^ _Nullable)(void))parameters __attribute__((swift_name("get(clazz:qualifier:parameters:)")));
- (id)getQualifier:(id<Weather_sharedKoin_coreQualifier> _Nullable)qualifier parameters:(Weather_sharedKoin_coreParametersHolder *(^ _Nullable)(void))parameters __attribute__((swift_name("get(qualifier:parameters:)")));
- (NSArray<id> *)getAll __attribute__((swift_name("getAll()")));
- (NSArray<id> *)getAllClazz:(id<Weather_sharedKotlinKClass>)clazz __attribute__((swift_name("getAll(clazz:)")));
- (Weather_sharedKoin_coreKoin *)getKoin __attribute__((swift_name("getKoin()")));
- (id _Nullable)getOrNullClazz:(id<Weather_sharedKotlinKClass>)clazz qualifier:(id<Weather_sharedKoin_coreQualifier> _Nullable)qualifier parameters:(Weather_sharedKoin_coreParametersHolder *(^ _Nullable)(void))parameters __attribute__((swift_name("getOrNull(clazz:qualifier:parameters:)")));
- (id _Nullable)getOrNullQualifier:(id<Weather_sharedKoin_coreQualifier> _Nullable)qualifier parameters:(Weather_sharedKoin_coreParametersHolder *(^ _Nullable)(void))parameters __attribute__((swift_name("getOrNull(qualifier:parameters:)")));
- (id)getPropertyKey:(NSString *)key __attribute__((swift_name("getProperty(key:)")));
- (id)getPropertyKey:(NSString *)key defaultValue:(id)defaultValue __attribute__((swift_name("getProperty(key:defaultValue:)")));
- (id _Nullable)getPropertyOrNullKey:(NSString *)key __attribute__((swift_name("getPropertyOrNull(key:)")));
- (Weather_sharedKoin_coreScope *)getScopeScopeID:(NSString *)scopeID __attribute__((swift_name("getScope(scopeID:)")));
- (id _Nullable)getSource __attribute__((swift_name("getSource()"))) __attribute__((deprecated("No need to use getSource(). You can an use get() directly.")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (id<Weather_sharedKotlinLazy>)injectQualifier:(id<Weather_sharedKoin_coreQualifier> _Nullable)qualifier mode:(Weather_sharedKotlinLazyThreadSafetyMode *)mode parameters:(Weather_sharedKoin_coreParametersHolder *(^ _Nullable)(void))parameters __attribute__((swift_name("inject(qualifier:mode:parameters:)")));
- (id<Weather_sharedKotlinLazy>)injectOrNullQualifier:(id<Weather_sharedKoin_coreQualifier> _Nullable)qualifier mode:(Weather_sharedKotlinLazyThreadSafetyMode *)mode parameters:(Weather_sharedKoin_coreParametersHolder *(^ _Nullable)(void))parameters __attribute__((swift_name("injectOrNull(qualifier:mode:parameters:)")));
- (BOOL)isNotClosed __attribute__((swift_name("isNotClosed()")));
- (void)linkToScopes:(Weather_sharedKotlinArray<Weather_sharedKoin_coreScope *> *)scopes __attribute__((swift_name("linkTo(scopes:)")));
- (void)registerCallbackCallback:(id<Weather_sharedKoin_coreScopeCallback>)callback __attribute__((swift_name("registerCallback(callback:)")));
- (NSString *)description __attribute__((swift_name("description()")));
- (void)unlinkScopes:(Weather_sharedKotlinArray<Weather_sharedKoin_coreScope *> *)scopes __attribute__((swift_name("unlink(scopes:)")));
@property (readonly) NSMutableArray<Weather_sharedKoin_coreParametersHolder *> *_parameterStack __attribute__((swift_name("_parameterStack")));
@property id _Nullable _source __attribute__((swift_name("_source")));
@property (readonly) BOOL closed __attribute__((swift_name("closed")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) BOOL isRoot __attribute__((swift_name("isRoot")));
@property (readonly) Weather_sharedKoin_coreLogger *logger __attribute__((swift_name("logger")));
@property (readonly) id<Weather_sharedKoin_coreQualifier> scopeQualifier __attribute__((swift_name("scopeQualifier")));
@end

__attribute__((swift_name("Koin_coreParametersHolder")))
@interface Weather_sharedKoin_coreParametersHolder : Weather_sharedBase
- (instancetype)initWith_values:(NSMutableArray<id> *)_values useIndexedValues:(Weather_sharedBoolean * _Nullable)useIndexedValues __attribute__((swift_name("init(_values:useIndexedValues:)"))) __attribute__((objc_designated_initializer));
- (Weather_sharedKoin_coreParametersHolder *)addValue:(id)value __attribute__((swift_name("add(value:)")));
- (id _Nullable)component1 __attribute__((swift_name("component1()")));
- (id _Nullable)component2 __attribute__((swift_name("component2()")));
- (id _Nullable)component3 __attribute__((swift_name("component3()")));
- (id _Nullable)component4 __attribute__((swift_name("component4()")));
- (id _Nullable)component5 __attribute__((swift_name("component5()")));
- (id _Nullable)elementAtI:(int32_t)i clazz:(id<Weather_sharedKotlinKClass>)clazz __attribute__((swift_name("elementAt(i:clazz:)")));
- (id)get __attribute__((swift_name("get()")));
- (id _Nullable)getI:(int32_t)i __attribute__((swift_name("get(i:)")));
- (id _Nullable)getOrNull __attribute__((swift_name("getOrNull()")));
- (id _Nullable)getOrNullClazz:(id<Weather_sharedKotlinKClass>)clazz __attribute__((swift_name("getOrNull(clazz:)")));
- (Weather_sharedKoin_coreParametersHolder *)insertIndex:(int32_t)index value:(id)value __attribute__((swift_name("insert(index:value:)")));
- (BOOL)isEmpty __attribute__((swift_name("isEmpty()")));
- (BOOL)isNotEmpty __attribute__((swift_name("isNotEmpty()")));
- (void)setI:(int32_t)i t:(id _Nullable)t __attribute__((swift_name("set(i:t:)")));
- (int32_t)size __attribute__((swift_name("size()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property int32_t index __attribute__((swift_name("index")));
@property (readonly) Weather_sharedBoolean * _Nullable useIndexedValues __attribute__((swift_name("useIndexedValues")));
@property (readonly) NSArray<id> *values __attribute__((swift_name("values")));
@end

__attribute__((swift_name("Koin_coreInstanceFactory")))
@interface Weather_sharedKoin_coreInstanceFactory<T> : Weather_sharedKoin_coreLockable
- (instancetype)initWithBeanDefinition:(Weather_sharedKoin_coreBeanDefinition<T> *)beanDefinition __attribute__((swift_name("init(beanDefinition:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
@property (class, readonly, getter=companion) Weather_sharedKoin_coreInstanceFactoryCompanion *companion __attribute__((swift_name("companion")));
- (T _Nullable)createContext:(Weather_sharedKoin_coreInstanceContext *)context __attribute__((swift_name("create(context:)")));
- (void)dropScope:(Weather_sharedKoin_coreScope * _Nullable)scope __attribute__((swift_name("drop(scope:)")));
- (void)dropAll __attribute__((swift_name("dropAll()")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (T _Nullable)getContext:(Weather_sharedKoin_coreInstanceContext *)context __attribute__((swift_name("get(context:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (BOOL)isCreatedContext:(Weather_sharedKoin_coreInstanceContext * _Nullable)context __attribute__((swift_name("isCreated(context:)")));
@property (readonly) Weather_sharedKoin_coreBeanDefinition<T> *beanDefinition __attribute__((swift_name("beanDefinition")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_coreSingleInstanceFactory")))
@interface Weather_sharedKoin_coreSingleInstanceFactory<T> : Weather_sharedKoin_coreInstanceFactory<T>
- (instancetype)initWithBeanDefinition:(Weather_sharedKoin_coreBeanDefinition<T> *)beanDefinition __attribute__((swift_name("init(beanDefinition:)"))) __attribute__((objc_designated_initializer));
- (T _Nullable)createContext:(Weather_sharedKoin_coreInstanceContext *)context __attribute__((swift_name("create(context:)")));
- (void)dropScope:(Weather_sharedKoin_coreScope * _Nullable)scope __attribute__((swift_name("drop(scope:)")));
- (void)dropAll __attribute__((swift_name("dropAll()")));
- (T _Nullable)getContext:(Weather_sharedKoin_coreInstanceContext *)context __attribute__((swift_name("get(context:)")));
- (BOOL)isCreatedContext:(Weather_sharedKoin_coreInstanceContext * _Nullable)context __attribute__((swift_name("isCreated(context:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_coreScopeDSL")))
@interface Weather_sharedKoin_coreScopeDSL : Weather_sharedBase
- (instancetype)initWithScopeQualifier:(id<Weather_sharedKoin_coreQualifier>)scopeQualifier module:(Weather_sharedKoin_coreModule *)module __attribute__((swift_name("init(scopeQualifier:module:)"))) __attribute__((objc_designated_initializer));
- (Weather_sharedKoin_coreKoinDefinition<id> *)factoryQualifier:(id<Weather_sharedKoin_coreQualifier> _Nullable)qualifier definition:(id _Nullable (^)(Weather_sharedKoin_coreScope *, Weather_sharedKoin_coreParametersHolder *))definition __attribute__((swift_name("factory(qualifier:definition:)")));
- (Weather_sharedKoin_coreKoinDefinition<id> *)scopedQualifier:(id<Weather_sharedKoin_coreQualifier> _Nullable)qualifier definition:(id _Nullable (^)(Weather_sharedKoin_coreScope *, Weather_sharedKoin_coreParametersHolder *))definition __attribute__((swift_name("scoped(qualifier:definition:)")));
@property (readonly) Weather_sharedKoin_coreModule *module __attribute__((swift_name("module")));
@property (readonly) id<Weather_sharedKoin_coreQualifier> scopeQualifier __attribute__((swift_name("scopeQualifier")));
@end

__attribute__((swift_name("Library_baseMutableRealm")))
@protocol Weather_sharedLibrary_baseMutableRealm <Weather_sharedLibrary_baseTypedRealm>
@required
- (void)cancelWrite __attribute__((swift_name("cancelWrite()")));
- (id<Weather_sharedLibrary_baseRealmObject>)doCopyToRealmInstance:(id<Weather_sharedLibrary_baseRealmObject>)instance updatePolicy:(Weather_sharedLibrary_baseUpdatePolicy *)updatePolicy __attribute__((swift_name("doCopyToRealm(instance:updatePolicy:)")));
- (void)deleteDeleteable:(id<Weather_sharedLibrary_baseDeleteable>)deleteable __attribute__((swift_name("delete(deleteable:)")));
- (void)deleteSchemaClass:(id<Weather_sharedKotlinKClass>)schemaClass __attribute__((swift_name("delete(schemaClass:)")));
- (void)deleteAll __attribute__((swift_name("deleteAll()")));
- (id<Weather_sharedLibrary_baseBaseRealmObject> _Nullable)findLatestObj:(id<Weather_sharedLibrary_baseBaseRealmObject>)obj __attribute__((swift_name("findLatest(obj:)")));
@end

__attribute__((swift_name("Library_baseRealmElementQuery")))
@protocol Weather_sharedLibrary_baseRealmElementQuery <Weather_sharedLibrary_baseDeleteable>
@required
- (id<Weather_sharedKotlinx_coroutines_coreFlow>)asFlow __attribute__((swift_name("asFlow()")));
- (NSArray<id<Weather_sharedLibrary_baseBaseRealmObject>> *)find __attribute__((swift_name("find()")));
@end

__attribute__((swift_name("Library_baseRealmQuery")))
@protocol Weather_sharedLibrary_baseRealmQuery <Weather_sharedLibrary_baseRealmElementQuery>
@required
- (id<Weather_sharedLibrary_baseRealmScalarQuery>)count __attribute__((swift_name("count()")));
- (NSString *)description_ __attribute__((swift_name("description_()")));
- (id<Weather_sharedLibrary_baseRealmQuery>)distinctProperty:(NSString *)property extraProperties:(Weather_sharedKotlinArray<NSString *> *)extraProperties __attribute__((swift_name("distinct(property:extraProperties:)")));
- (id<Weather_sharedLibrary_baseRealmSingleQuery>)first __attribute__((swift_name("first()")));
- (id<Weather_sharedLibrary_baseRealmQuery>)limitLimit:(int32_t)limit __attribute__((swift_name("limit(limit:)")));
- (id<Weather_sharedLibrary_baseRealmScalarNullableQuery>)maxProperty:(NSString *)property type:(id<Weather_sharedKotlinKClass>)type __attribute__((swift_name("max(property:type:)")));
- (id<Weather_sharedLibrary_baseRealmScalarNullableQuery>)minProperty:(NSString *)property type:(id<Weather_sharedKotlinKClass>)type __attribute__((swift_name("min(property:type:)")));
- (id<Weather_sharedLibrary_baseRealmQuery>)queryFilter:(NSString *)filter arguments:(Weather_sharedKotlinArray<id> *)arguments __attribute__((swift_name("query(filter:arguments:)")));
- (id<Weather_sharedLibrary_baseRealmQuery>)sortPropertyAndSortOrder:(Weather_sharedKotlinPair<NSString *, Weather_sharedLibrary_baseSort *> *)propertyAndSortOrder additionalPropertiesAndOrders:(Weather_sharedKotlinArray<Weather_sharedKotlinPair<NSString *, Weather_sharedLibrary_baseSort *> *> *)additionalPropertiesAndOrders __attribute__((swift_name("sort(propertyAndSortOrder:additionalPropertiesAndOrders:)")));
- (id<Weather_sharedLibrary_baseRealmQuery>)sortProperty:(NSString *)property sortOrder:(Weather_sharedLibrary_baseSort *)sortOrder __attribute__((swift_name("sort(property:sortOrder:)")));
- (id<Weather_sharedLibrary_baseRealmScalarQuery>)sumProperty:(NSString *)property type:(id<Weather_sharedKotlinKClass>)type __attribute__((swift_name("sum(property:type:)")));
@end

__attribute__((swift_name("KotlinKDeclarationContainer")))
@protocol Weather_sharedKotlinKDeclarationContainer
@required
@end

__attribute__((swift_name("KotlinKAnnotatedElement")))
@protocol Weather_sharedKotlinKAnnotatedElement
@required
@end


/**
 * @note annotations
 *   kotlin.SinceKotlin(version="1.1")
*/
__attribute__((swift_name("KotlinKClassifier")))
@protocol Weather_sharedKotlinKClassifier
@required
@end

__attribute__((swift_name("KotlinKClass")))
@protocol Weather_sharedKotlinKClass <Weather_sharedKotlinKDeclarationContainer, Weather_sharedKotlinKAnnotatedElement, Weather_sharedKotlinKClassifier>
@required

/**
 * @note annotations
 *   kotlin.SinceKotlin(version="1.1")
*/
- (BOOL)isInstanceValue:(id _Nullable)value __attribute__((swift_name("isInstance(value:)")));
@property (readonly) NSString * _Nullable qualifiedName __attribute__((swift_name("qualifiedName")));
@property (readonly) NSString * _Nullable simpleName __attribute__((swift_name("simpleName")));
@end

__attribute__((swift_name("Library_baseRealmSchema")))
@protocol Weather_sharedLibrary_baseRealmSchema
@required
- (id<Weather_sharedLibrary_baseRealmClass> _Nullable)getClassName:(NSString *)className __attribute__((swift_name("get(className:)")));
@property (readonly) id classes __attribute__((swift_name("classes")));
@end

__attribute__((swift_name("KotlinComparable")))
@protocol Weather_sharedKotlinComparable
@required
- (int32_t)compareToOther:(id _Nullable)other __attribute__((swift_name("compareTo(other:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Library_baseVersionId")))
@interface Weather_sharedLibrary_baseVersionId : Weather_sharedBase <Weather_sharedKotlinComparable>
- (instancetype)initWithVersion:(int64_t)version __attribute__((swift_name("init(version:)"))) __attribute__((objc_designated_initializer));
- (int32_t)compareToOther:(Weather_sharedLibrary_baseVersionId *)other __attribute__((swift_name("compareTo(other:)")));
- (Weather_sharedLibrary_baseVersionId *)doCopyVersion:(int64_t)version __attribute__((swift_name("doCopy(version:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int64_t version __attribute__((swift_name("version")));
@end

__attribute__((swift_name("Library_baseCompactOnLaunchCallback")))
@protocol Weather_sharedLibrary_baseCompactOnLaunchCallback
@required
- (BOOL)shouldCompactTotalBytes:(int64_t)totalBytes usedBytes:(int64_t)usedBytes __attribute__((swift_name("shouldCompact(totalBytes:usedBytes:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinByteArray")))
@interface Weather_sharedKotlinByteArray : Weather_sharedBase
+ (instancetype)arrayWithSize:(int32_t)size __attribute__((swift_name("init(size:)")));
+ (instancetype)arrayWithSize:(int32_t)size init:(Weather_sharedByte *(^)(Weather_sharedInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (int8_t)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (Weather_sharedKotlinByteIterator *)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(int8_t)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end

__attribute__((swift_name("Library_baseInitialDataCallback")))
@protocol Weather_sharedLibrary_baseInitialDataCallback
@required
- (void)write:(id<Weather_sharedLibrary_baseMutableRealm>)receiver __attribute__((swift_name("write(_:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Library_baseInitialRealmFileConfiguration")))
@interface Weather_sharedLibrary_baseInitialRealmFileConfiguration : Weather_sharedBase
- (instancetype)initWithAssetFile:(NSString *)assetFile checksum:(NSString * _Nullable)checksum __attribute__((swift_name("init(assetFile:checksum:)"))) __attribute__((objc_designated_initializer));
- (Weather_sharedLibrary_baseInitialRealmFileConfiguration *)doCopyAssetFile:(NSString *)assetFile checksum:(NSString * _Nullable)checksum __attribute__((swift_name("doCopy(assetFile:checksum:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *assetFile __attribute__((swift_name("assetFile")));
@property (readonly) NSString * _Nullable checksum __attribute__((swift_name("checksum")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Library_baseLogConfiguration")))
@interface Weather_sharedLibrary_baseLogConfiguration : Weather_sharedBase
- (instancetype)initWithLevel:(Weather_sharedLibrary_baseLogLevel *)level loggers:(NSArray<id<Weather_sharedLibrary_baseRealmLogger>> *)loggers __attribute__((swift_name("init(level:loggers:)"))) __attribute__((objc_designated_initializer)) __attribute__((deprecated("Use io.realm.kotlin.log.RealmLog instead.")));
- (Weather_sharedLibrary_baseLogConfiguration *)doCopyLevel:(Weather_sharedLibrary_baseLogLevel *)level loggers:(NSArray<id<Weather_sharedLibrary_baseRealmLogger>> *)loggers __attribute__((swift_name("doCopy(level:loggers:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) Weather_sharedLibrary_baseLogLevel *level __attribute__((swift_name("level")));
@property (readonly) NSArray<id<Weather_sharedLibrary_baseRealmLogger>> *loggers __attribute__((swift_name("loggers")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreCompositeEncoder")))
@protocol Weather_sharedKotlinx_serialization_coreCompositeEncoder
@required
- (void)encodeBooleanElementDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(BOOL)value __attribute__((swift_name("encodeBooleanElement(descriptor:index:value:)")));
- (void)encodeByteElementDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int8_t)value __attribute__((swift_name("encodeByteElement(descriptor:index:value:)")));
- (void)encodeCharElementDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(unichar)value __attribute__((swift_name("encodeCharElement(descriptor:index:value:)")));
- (void)encodeDoubleElementDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(double)value __attribute__((swift_name("encodeDoubleElement(descriptor:index:value:)")));
- (void)encodeFloatElementDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(float)value __attribute__((swift_name("encodeFloatElement(descriptor:index:value:)")));
- (id<Weather_sharedKotlinx_serialization_coreEncoder>)encodeInlineElementDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("encodeInlineElement(descriptor:index:)")));
- (void)encodeIntElementDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int32_t)value __attribute__((swift_name("encodeIntElement(descriptor:index:value:)")));
- (void)encodeLongElementDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int64_t)value __attribute__((swift_name("encodeLongElement(descriptor:index:value:)")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (void)encodeNullableSerializableElementDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index serializer:(id<Weather_sharedKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeNullableSerializableElement(descriptor:index:serializer:value:)")));
- (void)encodeSerializableElementDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index serializer:(id<Weather_sharedKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeSerializableElement(descriptor:index:serializer:value:)")));
- (void)encodeShortElementDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int16_t)value __attribute__((swift_name("encodeShortElement(descriptor:index:value:)")));
- (void)encodeStringElementDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(NSString *)value __attribute__((swift_name("encodeStringElement(descriptor:index:value:)")));
- (void)endStructureDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("endStructure(descriptor:)")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (BOOL)shouldEncodeElementDefaultDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("shouldEncodeElementDefault(descriptor:index:)")));
@property (readonly) Weather_sharedKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreSerializersModule")))
@interface Weather_sharedKotlinx_serialization_coreSerializersModule : Weather_sharedBase

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (void)dumpToCollector:(id<Weather_sharedKotlinx_serialization_coreSerializersModuleCollector>)collector __attribute__((swift_name("dumpTo(collector:)")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (id<Weather_sharedKotlinx_serialization_coreKSerializer> _Nullable)getContextualKClass:(id<Weather_sharedKotlinKClass>)kClass typeArgumentsSerializers:(NSArray<id<Weather_sharedKotlinx_serialization_coreKSerializer>> *)typeArgumentsSerializers __attribute__((swift_name("getContextual(kClass:typeArgumentsSerializers:)")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (id<Weather_sharedKotlinx_serialization_coreSerializationStrategy> _Nullable)getPolymorphicBaseClass:(id<Weather_sharedKotlinKClass>)baseClass value:(id)value __attribute__((swift_name("getPolymorphic(baseClass:value:)")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (id<Weather_sharedKotlinx_serialization_coreDeserializationStrategy> _Nullable)getPolymorphicBaseClass:(id<Weather_sharedKotlinKClass>)baseClass serializedClassName:(NSString * _Nullable)serializedClassName __attribute__((swift_name("getPolymorphic(baseClass:serializedClassName:)")));
@end

__attribute__((swift_name("KotlinAnnotation")))
@protocol Weather_sharedKotlinAnnotation
@required
@end


/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
__attribute__((swift_name("Kotlinx_serialization_coreSerialKind")))
@interface Weather_sharedKotlinx_serialization_coreSerialKind : Weather_sharedBase
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreCompositeDecoder")))
@protocol Weather_sharedKotlinx_serialization_coreCompositeDecoder
@required
- (BOOL)decodeBooleanElementDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeBooleanElement(descriptor:index:)")));
- (int8_t)decodeByteElementDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeByteElement(descriptor:index:)")));
- (unichar)decodeCharElementDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeCharElement(descriptor:index:)")));
- (int32_t)decodeCollectionSizeDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("decodeCollectionSize(descriptor:)")));
- (double)decodeDoubleElementDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeDoubleElement(descriptor:index:)")));
- (int32_t)decodeElementIndexDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("decodeElementIndex(descriptor:)")));
- (float)decodeFloatElementDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeFloatElement(descriptor:index:)")));
- (id<Weather_sharedKotlinx_serialization_coreDecoder>)decodeInlineElementDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeInlineElement(descriptor:index:)")));
- (int32_t)decodeIntElementDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeIntElement(descriptor:index:)")));
- (int64_t)decodeLongElementDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeLongElement(descriptor:index:)")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (id _Nullable)decodeNullableSerializableElementDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index deserializer:(id<Weather_sharedKotlinx_serialization_coreDeserializationStrategy>)deserializer previousValue:(id _Nullable)previousValue __attribute__((swift_name("decodeNullableSerializableElement(descriptor:index:deserializer:previousValue:)")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (BOOL)decodeSequentially __attribute__((swift_name("decodeSequentially()")));
- (id _Nullable)decodeSerializableElementDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index deserializer:(id<Weather_sharedKotlinx_serialization_coreDeserializationStrategy>)deserializer previousValue:(id _Nullable)previousValue __attribute__((swift_name("decodeSerializableElement(descriptor:index:deserializer:previousValue:)")));
- (int16_t)decodeShortElementDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeShortElement(descriptor:index:)")));
- (NSString *)decodeStringElementDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeStringElement(descriptor:index:)")));
- (void)endStructureDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("endStructure(descriptor:)")));
@property (readonly) Weather_sharedKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinNothing")))
@interface Weather_sharedKotlinNothing : Weather_sharedBase
@end


/**
 * @note annotations
 *   kotlin.SinceKotlin(version="1.3")
*/
__attribute__((swift_name("KotlinCoroutineContext")))
@protocol Weather_sharedKotlinCoroutineContext
@required
- (id _Nullable)foldInitial:(id _Nullable)initial operation:(id _Nullable (^)(id _Nullable, id<Weather_sharedKotlinCoroutineContextElement>))operation __attribute__((swift_name("fold(initial:operation:)")));
- (id<Weather_sharedKotlinCoroutineContextElement> _Nullable)getKey:(id<Weather_sharedKotlinCoroutineContextKey>)key __attribute__((swift_name("get(key:)")));
- (id<Weather_sharedKotlinCoroutineContext>)minusKeyKey:(id<Weather_sharedKotlinCoroutineContextKey>)key __attribute__((swift_name("minusKey(key:)")));
- (id<Weather_sharedKotlinCoroutineContext>)plusContext:(id<Weather_sharedKotlinCoroutineContext>)context __attribute__((swift_name("plus(context:)")));
@end

__attribute__((swift_name("KotlinIterator")))
@protocol Weather_sharedKotlinIterator
@required
- (BOOL)hasNext __attribute__((swift_name("hasNext()")));
- (id _Nullable)next __attribute__((swift_name("next()")));
@end

__attribute__((swift_name("Core_sharedCoreConfig")))
@protocol Weather_sharedCore_sharedCoreConfig
@required
@property (readonly) NSString * _Nullable apiAuthPath __attribute__((swift_name("apiAuthPath")));
@property (readonly) NSString * _Nullable apiChannel __attribute__((swift_name("apiChannel")));
@property (readonly) NSString * _Nullable apiRefreshTokenPath __attribute__((swift_name("apiRefreshTokenPath")));
@property (readonly) NSString * _Nullable headerChannel __attribute__((swift_name("headerChannel")));
@property (readonly) NSString * _Nullable headerDeviceId __attribute__((swift_name("headerDeviceId")));
@property (readonly) NSString * _Nullable headerLanguage __attribute__((swift_name("headerLanguage")));
@property (readonly) NSString * _Nullable headerOs __attribute__((swift_name("headerOs")));
@property (readonly) NSString * _Nullable headerVersion __attribute__((swift_name("headerVersion")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreSerialFormat")))
@protocol Weather_sharedKotlinx_serialization_coreSerialFormat
@required
@property (readonly) Weather_sharedKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreStringFormat")))
@protocol Weather_sharedKotlinx_serialization_coreStringFormat <Weather_sharedKotlinx_serialization_coreSerialFormat>
@required
- (id _Nullable)decodeFromStringDeserializer:(id<Weather_sharedKotlinx_serialization_coreDeserializationStrategy>)deserializer string:(NSString *)string __attribute__((swift_name("decodeFromString(deserializer:string:)")));
- (NSString *)encodeToStringSerializer:(id<Weather_sharedKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeToString(serializer:value:)")));
@end

__attribute__((swift_name("Kotlinx_serialization_jsonJson")))
@interface Weather_sharedKotlinx_serialization_jsonJson : Weather_sharedBase <Weather_sharedKotlinx_serialization_coreStringFormat>
@property (class, readonly, getter=companion) Weather_sharedKotlinx_serialization_jsonJsonDefault *companion __attribute__((swift_name("companion")));
- (id _Nullable)decodeFromJsonElementDeserializer:(id<Weather_sharedKotlinx_serialization_coreDeserializationStrategy>)deserializer element:(Weather_sharedKotlinx_serialization_jsonJsonElement *)element __attribute__((swift_name("decodeFromJsonElement(deserializer:element:)")));
- (id _Nullable)decodeFromStringString:(NSString *)string __attribute__((swift_name("decodeFromString(string:)")));
- (id _Nullable)decodeFromStringDeserializer:(id<Weather_sharedKotlinx_serialization_coreDeserializationStrategy>)deserializer string:(NSString *)string __attribute__((swift_name("decodeFromString(deserializer:string:)")));
- (Weather_sharedKotlinx_serialization_jsonJsonElement *)encodeToJsonElementSerializer:(id<Weather_sharedKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeToJsonElement(serializer:value:)")));
- (NSString *)encodeToStringSerializer:(id<Weather_sharedKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeToString(serializer:value:)")));
- (Weather_sharedKotlinx_serialization_jsonJsonElement *)parseToJsonElementString:(NSString *)string __attribute__((swift_name("parseToJsonElement(string:)")));
@property (readonly) Weather_sharedKotlinx_serialization_jsonJsonConfiguration *configuration __attribute__((swift_name("configuration")));
@property (readonly) Weather_sharedKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end

__attribute__((swift_name("Core_sharedRefreshTokenUseCase")))
@protocol Weather_sharedCore_sharedRefreshTokenUseCase <Weather_sharedCore_sharedCoreUseCase>
@required
@property (getter=doNewRefreshToken) NSString *newRefreshToken __attribute__((swift_name("newRefreshToken")));
@property (getter=doNewToken) NSString *newToken __attribute__((swift_name("newToken")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpURLProtocol")))
@interface Weather_sharedKtor_httpURLProtocol : Weather_sharedBase
- (instancetype)initWithName:(NSString *)name defaultPort:(int32_t)defaultPort __attribute__((swift_name("init(name:defaultPort:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Weather_sharedKtor_httpURLProtocolCompanion *companion __attribute__((swift_name("companion")));
- (Weather_sharedKtor_httpURLProtocol *)doCopyName:(NSString *)name defaultPort:(int32_t)defaultPort __attribute__((swift_name("doCopy(name:defaultPort:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t defaultPort __attribute__((swift_name("defaultPort")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_coreKoin")))
@interface Weather_sharedKoin_coreKoin : Weather_sharedBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)close __attribute__((swift_name("close()")));
- (void)createEagerInstances __attribute__((swift_name("createEagerInstances()")));
- (Weather_sharedKoin_coreScope *)createScopeT:(id<Weather_sharedKoin_coreKoinScopeComponent>)t __attribute__((swift_name("createScope(t:)")));
- (Weather_sharedKoin_coreScope *)createScopeScopeId:(NSString *)scopeId __attribute__((swift_name("createScope(scopeId:)")));
- (Weather_sharedKoin_coreScope *)createScopeScopeId:(NSString *)scopeId source:(id _Nullable)source __attribute__((swift_name("createScope(scopeId:source:)")));
- (Weather_sharedKoin_coreScope *)createScopeScopeId:(NSString *)scopeId qualifier:(id<Weather_sharedKoin_coreQualifier>)qualifier source:(id _Nullable)source __attribute__((swift_name("createScope(scopeId:qualifier:source:)")));
- (void)declareInstance:(id _Nullable)instance qualifier:(id<Weather_sharedKoin_coreQualifier> _Nullable)qualifier secondaryTypes:(NSArray<id<Weather_sharedKotlinKClass>> *)secondaryTypes allowOverride:(BOOL)allowOverride __attribute__((swift_name("declare(instance:qualifier:secondaryTypes:allowOverride:)")));
- (void)deletePropertyKey:(NSString *)key __attribute__((swift_name("deleteProperty(key:)")));
- (void)deleteScopeScopeId:(NSString *)scopeId __attribute__((swift_name("deleteScope(scopeId:)")));
- (id _Nullable)getClazz:(id<Weather_sharedKotlinKClass>)clazz qualifier:(id<Weather_sharedKoin_coreQualifier> _Nullable)qualifier parameters:(Weather_sharedKoin_coreParametersHolder *(^ _Nullable)(void))parameters __attribute__((swift_name("get(clazz:qualifier:parameters:)")));
- (id)getQualifier:(id<Weather_sharedKoin_coreQualifier> _Nullable)qualifier parameters:(Weather_sharedKoin_coreParametersHolder *(^ _Nullable)(void))parameters __attribute__((swift_name("get(qualifier:parameters:)")));
- (NSArray<id> *)getAll __attribute__((swift_name("getAll()")));
- (Weather_sharedKoin_coreScope *)getOrCreateScopeScopeId:(NSString *)scopeId __attribute__((swift_name("getOrCreateScope(scopeId:)")));
- (Weather_sharedKoin_coreScope *)getOrCreateScopeScopeId:(NSString *)scopeId qualifier:(id<Weather_sharedKoin_coreQualifier>)qualifier source:(id _Nullable)source __attribute__((swift_name("getOrCreateScope(scopeId:qualifier:source:)")));
- (id _Nullable)getOrNullClazz:(id<Weather_sharedKotlinKClass>)clazz qualifier:(id<Weather_sharedKoin_coreQualifier> _Nullable)qualifier parameters:(Weather_sharedKoin_coreParametersHolder *(^ _Nullable)(void))parameters __attribute__((swift_name("getOrNull(clazz:qualifier:parameters:)")));
- (id _Nullable)getOrNullQualifier:(id<Weather_sharedKoin_coreQualifier> _Nullable)qualifier parameters:(Weather_sharedKoin_coreParametersHolder *(^ _Nullable)(void))parameters __attribute__((swift_name("getOrNull(qualifier:parameters:)")));
- (id _Nullable)getPropertyKey:(NSString *)key __attribute__((swift_name("getProperty(key:)")));
- (id)getPropertyKey:(NSString *)key defaultValue:(id)defaultValue __attribute__((swift_name("getProperty(key:defaultValue:)")));
- (Weather_sharedKoin_coreScope *)getScopeScopeId:(NSString *)scopeId __attribute__((swift_name("getScope(scopeId:)")));
- (Weather_sharedKoin_coreScope * _Nullable)getScopeOrNullScopeId:(NSString *)scopeId __attribute__((swift_name("getScopeOrNull(scopeId:)")));
- (id<Weather_sharedKotlinLazy>)injectQualifier:(id<Weather_sharedKoin_coreQualifier> _Nullable)qualifier mode:(Weather_sharedKotlinLazyThreadSafetyMode *)mode parameters:(Weather_sharedKoin_coreParametersHolder *(^ _Nullable)(void))parameters __attribute__((swift_name("inject(qualifier:mode:parameters:)")));
- (id<Weather_sharedKotlinLazy>)injectOrNullQualifier:(id<Weather_sharedKoin_coreQualifier> _Nullable)qualifier mode:(Weather_sharedKotlinLazyThreadSafetyMode *)mode parameters:(Weather_sharedKoin_coreParametersHolder *(^ _Nullable)(void))parameters __attribute__((swift_name("injectOrNull(qualifier:mode:parameters:)")));
- (void)loadModulesModules:(NSArray<Weather_sharedKoin_coreModule *> *)modules allowOverride:(BOOL)allowOverride __attribute__((swift_name("loadModules(modules:allowOverride:)")));
- (void)setPropertyKey:(NSString *)key value:(id)value __attribute__((swift_name("setProperty(key:value:)")));
- (void)setupLoggerLogger:(Weather_sharedKoin_coreLogger *)logger __attribute__((swift_name("setupLogger(logger:)")));
- (void)unloadModulesModules:(NSArray<Weather_sharedKoin_coreModule *> *)modules __attribute__((swift_name("unloadModules(modules:)")));
@property (readonly) Weather_sharedKoin_coreExtensionManager *extensionManager __attribute__((swift_name("extensionManager")));
@property (readonly) Weather_sharedKoin_coreInstanceRegistry *instanceRegistry __attribute__((swift_name("instanceRegistry")));
@property (readonly) Weather_sharedKoin_coreLogger *logger __attribute__((swift_name("logger")));
@property (readonly) Weather_sharedKoin_corePropertyRegistry *propertyRegistry __attribute__((swift_name("propertyRegistry")));
@property (readonly) Weather_sharedKoin_coreScopeRegistry *scopeRegistry __attribute__((swift_name("scopeRegistry")));
@end

__attribute__((swift_name("KotlinLazy")))
@protocol Weather_sharedKotlinLazy
@required
- (BOOL)isInitialized __attribute__((swift_name("isInitialized()")));
@property (readonly) id _Nullable value __attribute__((swift_name("value")));
@end

__attribute__((swift_name("KotlinEnum")))
@interface Weather_sharedKotlinEnum<E> : Weather_sharedBase <Weather_sharedKotlinComparable>
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Weather_sharedKotlinEnumCompanion *companion __attribute__((swift_name("companion")));
- (int32_t)compareToOther:(E)other __attribute__((swift_name("compareTo(other:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) int32_t ordinal __attribute__((swift_name("ordinal")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinLazyThreadSafetyMode")))
@interface Weather_sharedKotlinLazyThreadSafetyMode : Weather_sharedKotlinEnum<Weather_sharedKotlinLazyThreadSafetyMode *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) Weather_sharedKotlinLazyThreadSafetyMode *synchronized __attribute__((swift_name("synchronized")));
@property (class, readonly) Weather_sharedKotlinLazyThreadSafetyMode *publication __attribute__((swift_name("publication")));
@property (class, readonly) Weather_sharedKotlinLazyThreadSafetyMode *none __attribute__((swift_name("none")));
+ (Weather_sharedKotlinArray<Weather_sharedKotlinLazyThreadSafetyMode *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<Weather_sharedKotlinLazyThreadSafetyMode *> *entries __attribute__((swift_name("entries")));
@end

__attribute__((swift_name("Koin_coreScopeCallback")))
@protocol Weather_sharedKoin_coreScopeCallback
@required
- (void)onScopeCloseScope:(Weather_sharedKoin_coreScope *)scope __attribute__((swift_name("onScopeClose(scope:)")));
@end

__attribute__((swift_name("Koin_coreLogger")))
@interface Weather_sharedKoin_coreLogger : Weather_sharedBase
- (instancetype)initWithLevel:(Weather_sharedKoin_coreLevel *)level __attribute__((swift_name("init(level:)"))) __attribute__((objc_designated_initializer));
- (void)debugMsg:(NSString *)msg __attribute__((swift_name("debug(msg:)")));
- (void)displayLevel:(Weather_sharedKoin_coreLevel *)level msg:(NSString *)msg __attribute__((swift_name("display(level:msg:)")));
- (void)errorMsg:(NSString *)msg __attribute__((swift_name("error(msg:)")));
- (void)infoMsg:(NSString *)msg __attribute__((swift_name("info(msg:)")));
- (BOOL)isAtLvl:(Weather_sharedKoin_coreLevel *)lvl __attribute__((swift_name("isAt(lvl:)")));
- (void)logLvl:(Weather_sharedKoin_coreLevel *)lvl msg:(NSString *(^)(void))msg __attribute__((swift_name("log(lvl:msg:)")));
- (void)logLvl:(Weather_sharedKoin_coreLevel *)lvl msg_:(NSString *)msg __attribute__((swift_name("log(lvl:msg_:)")));
- (void)warnMsg:(NSString *)msg __attribute__((swift_name("warn(msg:)")));
@property Weather_sharedKoin_coreLevel *level __attribute__((swift_name("level")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_coreBeanDefinition")))
@interface Weather_sharedKoin_coreBeanDefinition<T> : Weather_sharedBase
- (instancetype)initWithScopeQualifier:(id<Weather_sharedKoin_coreQualifier>)scopeQualifier primaryType:(id<Weather_sharedKotlinKClass>)primaryType qualifier:(id<Weather_sharedKoin_coreQualifier> _Nullable)qualifier definition:(T _Nullable (^)(Weather_sharedKoin_coreScope *, Weather_sharedKoin_coreParametersHolder *))definition kind:(Weather_sharedKoin_coreKind *)kind secondaryTypes:(NSArray<id<Weather_sharedKotlinKClass>> *)secondaryTypes __attribute__((swift_name("init(scopeQualifier:primaryType:qualifier:definition:kind:secondaryTypes:)"))) __attribute__((objc_designated_initializer));
- (Weather_sharedKoin_coreBeanDefinition<T> *)doCopyScopeQualifier:(id<Weather_sharedKoin_coreQualifier>)scopeQualifier primaryType:(id<Weather_sharedKotlinKClass>)primaryType qualifier:(id<Weather_sharedKoin_coreQualifier> _Nullable)qualifier definition:(T _Nullable (^)(Weather_sharedKoin_coreScope *, Weather_sharedKoin_coreParametersHolder *))definition kind:(Weather_sharedKoin_coreKind *)kind secondaryTypes:(NSArray<id<Weather_sharedKotlinKClass>> *)secondaryTypes __attribute__((swift_name("doCopy(scopeQualifier:primaryType:qualifier:definition:kind:secondaryTypes:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (BOOL)hasTypeClazz:(id<Weather_sharedKotlinKClass>)clazz __attribute__((swift_name("hasType(clazz:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (BOOL)isClazz:(id<Weather_sharedKotlinKClass>)clazz qualifier:(id<Weather_sharedKoin_coreQualifier> _Nullable)qualifier scopeDefinition:(id<Weather_sharedKoin_coreQualifier>)scopeDefinition __attribute__((swift_name("is(clazz:qualifier:scopeDefinition:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property Weather_sharedKoin_coreCallbacks<T> *callbacks __attribute__((swift_name("callbacks")));
@property (readonly) T _Nullable (^definition)(Weather_sharedKoin_coreScope *, Weather_sharedKoin_coreParametersHolder *) __attribute__((swift_name("definition")));
@property (readonly) Weather_sharedKoin_coreKind *kind __attribute__((swift_name("kind")));
@property (readonly) id<Weather_sharedKotlinKClass> primaryType __attribute__((swift_name("primaryType")));
@property id<Weather_sharedKoin_coreQualifier> _Nullable qualifier __attribute__((swift_name("qualifier")));
@property (readonly) id<Weather_sharedKoin_coreQualifier> scopeQualifier __attribute__((swift_name("scopeQualifier")));
@property NSArray<id<Weather_sharedKotlinKClass>> *secondaryTypes __attribute__((swift_name("secondaryTypes")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_coreInstanceFactoryCompanion")))
@interface Weather_sharedKoin_coreInstanceFactoryCompanion : Weather_sharedBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Weather_sharedKoin_coreInstanceFactoryCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) NSString *ERROR_SEPARATOR __attribute__((swift_name("ERROR_SEPARATOR")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_coreInstanceContext")))
@interface Weather_sharedKoin_coreInstanceContext : Weather_sharedBase
- (instancetype)initWithLogger:(Weather_sharedKoin_coreLogger *)logger scope:(Weather_sharedKoin_coreScope *)scope parameters:(Weather_sharedKoin_coreParametersHolder * _Nullable)parameters __attribute__((swift_name("init(logger:scope:parameters:)"))) __attribute__((objc_designated_initializer));
@property (readonly) Weather_sharedKoin_coreLogger *logger __attribute__((swift_name("logger")));
@property (readonly) Weather_sharedKoin_coreParametersHolder * _Nullable parameters __attribute__((swift_name("parameters")));
@property (readonly) Weather_sharedKoin_coreScope *scope __attribute__((swift_name("scope")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Library_baseUpdatePolicy")))
@interface Weather_sharedLibrary_baseUpdatePolicy : Weather_sharedKotlinEnum<Weather_sharedLibrary_baseUpdatePolicy *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) Weather_sharedLibrary_baseUpdatePolicy *error __attribute__((swift_name("error")));
@property (class, readonly) Weather_sharedLibrary_baseUpdatePolicy *all __attribute__((swift_name("all")));
+ (Weather_sharedKotlinArray<Weather_sharedLibrary_baseUpdatePolicy *> *)values __attribute__((swift_name("values()")));
@end

__attribute__((swift_name("Library_baseRealmScalarQuery")))
@protocol Weather_sharedLibrary_baseRealmScalarQuery
@required
- (id<Weather_sharedKotlinx_coroutines_coreFlow>)asFlow __attribute__((swift_name("asFlow()")));
- (id _Nullable)find __attribute__((swift_name("find()")));
@end

__attribute__((swift_name("Library_baseRealmSingleQuery")))
@protocol Weather_sharedLibrary_baseRealmSingleQuery <Weather_sharedLibrary_baseDeleteable>
@required
- (id<Weather_sharedKotlinx_coroutines_coreFlow>)asFlow __attribute__((swift_name("asFlow()")));
- (id<Weather_sharedLibrary_baseBaseRealmObject> _Nullable)find __attribute__((swift_name("find()")));
@end

__attribute__((swift_name("Library_baseRealmScalarNullableQuery")))
@protocol Weather_sharedLibrary_baseRealmScalarNullableQuery <Weather_sharedLibrary_baseRealmScalarQuery>
@required
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Library_baseSort")))
@interface Weather_sharedLibrary_baseSort : Weather_sharedKotlinEnum<Weather_sharedLibrary_baseSort *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) Weather_sharedLibrary_baseSort *ascending __attribute__((swift_name("ascending")));
@property (class, readonly) Weather_sharedLibrary_baseSort *descending __attribute__((swift_name("descending")));
+ (Weather_sharedKotlinArray<Weather_sharedLibrary_baseSort *> *)values __attribute__((swift_name("values()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinPair")))
@interface Weather_sharedKotlinPair<__covariant A, __covariant B> : Weather_sharedBase
- (instancetype)initWithFirst:(A _Nullable)first second:(B _Nullable)second __attribute__((swift_name("init(first:second:)"))) __attribute__((objc_designated_initializer));
- (Weather_sharedKotlinPair<A, B> *)doCopyFirst:(A _Nullable)first second:(B _Nullable)second __attribute__((swift_name("doCopy(first:second:)")));
- (BOOL)equalsOther:(id _Nullable)other __attribute__((swift_name("equals(other:)")));
- (int32_t)hashCode __attribute__((swift_name("hashCode()")));
- (NSString *)toString __attribute__((swift_name("toString()")));
@property (readonly) A _Nullable first __attribute__((swift_name("first")));
@property (readonly) B _Nullable second __attribute__((swift_name("second")));
@end

__attribute__((swift_name("Library_baseRealmClass")))
@protocol Weather_sharedLibrary_baseRealmClass
@required
- (id<Weather_sharedLibrary_baseRealmProperty> _Nullable)getKey_:(NSString *)key __attribute__((swift_name("get(key_:)")));
@property (readonly) BOOL isEmbedded __attribute__((swift_name("isEmbedded"))) __attribute__((deprecated("This property has been deprecated.")));
@property (readonly) Weather_sharedLibrary_baseRealmClassKind *kind __attribute__((swift_name("kind")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) id<Weather_sharedLibrary_baseRealmProperty> _Nullable primaryKey __attribute__((swift_name("primaryKey")));
@property (readonly) id properties __attribute__((swift_name("properties")));
@end

__attribute__((swift_name("KotlinByteIterator")))
@interface Weather_sharedKotlinByteIterator : Weather_sharedBase <Weather_sharedKotlinIterator>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (Weather_sharedByte *)next __attribute__((swift_name("next()")));
- (int8_t)nextByte __attribute__((swift_name("nextByte()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Library_baseLogLevel")))
@interface Weather_sharedLibrary_baseLogLevel : Weather_sharedKotlinEnum<Weather_sharedLibrary_baseLogLevel *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) Weather_sharedLibrary_baseLogLevel *all __attribute__((swift_name("all")));
@property (class, readonly) Weather_sharedLibrary_baseLogLevel *trace __attribute__((swift_name("trace")));
@property (class, readonly) Weather_sharedLibrary_baseLogLevel *debug __attribute__((swift_name("debug")));
@property (class, readonly) Weather_sharedLibrary_baseLogLevel *info __attribute__((swift_name("info")));
@property (class, readonly) Weather_sharedLibrary_baseLogLevel *warn __attribute__((swift_name("warn")));
@property (class, readonly) Weather_sharedLibrary_baseLogLevel *error __attribute__((swift_name("error")));
@property (class, readonly) Weather_sharedLibrary_baseLogLevel *wtf __attribute__((swift_name("wtf")));
@property (class, readonly) Weather_sharedLibrary_baseLogLevel *none __attribute__((swift_name("none")));
+ (Weather_sharedKotlinArray<Weather_sharedLibrary_baseLogLevel *> *)values __attribute__((swift_name("values()")));
@property (readonly) int32_t priority __attribute__((swift_name("priority")));
@end

__attribute__((swift_name("Library_baseRealmLogger")))
@protocol Weather_sharedLibrary_baseRealmLogger
@required
- (void)logLevel:(Weather_sharedLibrary_baseLogLevel *)level message:(NSString *)message __attribute__((swift_name("log(level:message:)")));
- (void)logLevel:(Weather_sharedLibrary_baseLogLevel *)level throwable:(Weather_sharedKotlinThrowable * _Nullable)throwable message:(NSString * _Nullable)message args:(Weather_sharedKotlinArray<id> *)args __attribute__((swift_name("log(level:throwable:message:args:)")));
@property (readonly) Weather_sharedLibrary_baseLogLevel *level __attribute__((swift_name("level")));
@property (readonly) NSString *tag __attribute__((swift_name("tag")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
__attribute__((swift_name("Kotlinx_serialization_coreSerializersModuleCollector")))
@protocol Weather_sharedKotlinx_serialization_coreSerializersModuleCollector
@required
- (void)contextualKClass:(id<Weather_sharedKotlinKClass>)kClass provider:(id<Weather_sharedKotlinx_serialization_coreKSerializer> (^)(NSArray<id<Weather_sharedKotlinx_serialization_coreKSerializer>> *))provider __attribute__((swift_name("contextual(kClass:provider:)")));
- (void)contextualKClass:(id<Weather_sharedKotlinKClass>)kClass serializer:(id<Weather_sharedKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("contextual(kClass:serializer:)")));
- (void)polymorphicBaseClass:(id<Weather_sharedKotlinKClass>)baseClass actualClass:(id<Weather_sharedKotlinKClass>)actualClass actualSerializer:(id<Weather_sharedKotlinx_serialization_coreKSerializer>)actualSerializer __attribute__((swift_name("polymorphic(baseClass:actualClass:actualSerializer:)")));
- (void)polymorphicDefaultBaseClass:(id<Weather_sharedKotlinKClass>)baseClass defaultDeserializerProvider:(id<Weather_sharedKotlinx_serialization_coreDeserializationStrategy> _Nullable (^)(NSString * _Nullable))defaultDeserializerProvider __attribute__((swift_name("polymorphicDefault(baseClass:defaultDeserializerProvider:)"))) __attribute__((deprecated("Deprecated in favor of function with more precise name: polymorphicDefaultDeserializer")));
- (void)polymorphicDefaultDeserializerBaseClass:(id<Weather_sharedKotlinKClass>)baseClass defaultDeserializerProvider:(id<Weather_sharedKotlinx_serialization_coreDeserializationStrategy> _Nullable (^)(NSString * _Nullable))defaultDeserializerProvider __attribute__((swift_name("polymorphicDefaultDeserializer(baseClass:defaultDeserializerProvider:)")));
- (void)polymorphicDefaultSerializerBaseClass:(id<Weather_sharedKotlinKClass>)baseClass defaultSerializerProvider:(id<Weather_sharedKotlinx_serialization_coreSerializationStrategy> _Nullable (^)(id))defaultSerializerProvider __attribute__((swift_name("polymorphicDefaultSerializer(baseClass:defaultSerializerProvider:)")));
@end

__attribute__((swift_name("KotlinCoroutineContextElement")))
@protocol Weather_sharedKotlinCoroutineContextElement <Weather_sharedKotlinCoroutineContext>
@required
@property (readonly) id<Weather_sharedKotlinCoroutineContextKey> key __attribute__((swift_name("key")));
@end

__attribute__((swift_name("KotlinCoroutineContextKey")))
@protocol Weather_sharedKotlinCoroutineContextKey
@required
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_serialization_jsonJson.Default")))
@interface Weather_sharedKotlinx_serialization_jsonJsonDefault : Weather_sharedKotlinx_serialization_jsonJson
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)default_ __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Weather_sharedKotlinx_serialization_jsonJsonDefault *shared __attribute__((swift_name("shared")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable(with=NormalClass(value=kotlinx/serialization/json/JsonElementSerializer))
*/
__attribute__((swift_name("Kotlinx_serialization_jsonJsonElement")))
@interface Weather_sharedKotlinx_serialization_jsonJsonElement : Weather_sharedBase
@property (class, readonly, getter=companion) Weather_sharedKotlinx_serialization_jsonJsonElementCompanion *companion __attribute__((swift_name("companion")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_serialization_jsonJsonConfiguration")))
@interface Weather_sharedKotlinx_serialization_jsonJsonConfiguration : Weather_sharedBase
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL allowSpecialFloatingPointValues __attribute__((swift_name("allowSpecialFloatingPointValues")));
@property (readonly) BOOL allowStructuredMapKeys __attribute__((swift_name("allowStructuredMapKeys")));
@property (readonly) NSString *classDiscriminator __attribute__((swift_name("classDiscriminator")));
@property (readonly) BOOL coerceInputValues __attribute__((swift_name("coerceInputValues")));
@property (readonly) BOOL encodeDefaults __attribute__((swift_name("encodeDefaults")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
@property (readonly) BOOL explicitNulls __attribute__((swift_name("explicitNulls")));
@property (readonly) BOOL ignoreUnknownKeys __attribute__((swift_name("ignoreUnknownKeys")));
@property (readonly) BOOL isLenient __attribute__((swift_name("isLenient")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
@property (readonly) id<Weather_sharedKotlinx_serialization_jsonJsonNamingStrategy> _Nullable namingStrategy __attribute__((swift_name("namingStrategy")));
@property (readonly) BOOL prettyPrint __attribute__((swift_name("prettyPrint")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
@property (readonly) NSString *prettyPrintIndent __attribute__((swift_name("prettyPrintIndent")));
@property (readonly) BOOL useAlternativeNames __attribute__((swift_name("useAlternativeNames")));
@property (readonly) BOOL useArrayPolymorphism __attribute__((swift_name("useArrayPolymorphism")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpURLProtocol.Companion")))
@interface Weather_sharedKtor_httpURLProtocolCompanion : Weather_sharedBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Weather_sharedKtor_httpURLProtocolCompanion *shared __attribute__((swift_name("shared")));
- (Weather_sharedKtor_httpURLProtocol *)createOrDefaultName:(NSString *)name __attribute__((swift_name("createOrDefault(name:)")));
@property (readonly) Weather_sharedKtor_httpURLProtocol *HTTP __attribute__((swift_name("HTTP")));
@property (readonly) Weather_sharedKtor_httpURLProtocol *HTTPS __attribute__((swift_name("HTTPS")));
@property (readonly) Weather_sharedKtor_httpURLProtocol *SOCKS __attribute__((swift_name("SOCKS")));
@property (readonly) Weather_sharedKtor_httpURLProtocol *WS __attribute__((swift_name("WS")));
@property (readonly) Weather_sharedKtor_httpURLProtocol *WSS __attribute__((swift_name("WSS")));
@property (readonly) NSDictionary<NSString *, Weather_sharedKtor_httpURLProtocol *> *byName __attribute__((swift_name("byName")));
@end

__attribute__((swift_name("Koin_coreKoinComponent")))
@protocol Weather_sharedKoin_coreKoinComponent
@required
- (Weather_sharedKoin_coreKoin *)getKoin __attribute__((swift_name("getKoin()")));
@end

__attribute__((swift_name("Koin_coreKoinScopeComponent")))
@protocol Weather_sharedKoin_coreKoinScopeComponent <Weather_sharedKoin_coreKoinComponent>
@required
- (void)closeScope __attribute__((swift_name("closeScope()"))) __attribute__((deprecated("not used internaly anymore")));
@property (readonly) Weather_sharedKoin_coreScope *scope __attribute__((swift_name("scope")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_coreExtensionManager")))
@interface Weather_sharedKoin_coreExtensionManager : Weather_sharedBase
- (instancetype)initWith_koin:(Weather_sharedKoin_coreKoin *)_koin __attribute__((swift_name("init(_koin:)"))) __attribute__((objc_designated_initializer));
- (void)close __attribute__((swift_name("close()")));
- (id<Weather_sharedKoin_coreKoinExtension>)getExtensionId:(NSString *)id __attribute__((swift_name("getExtension(id:)")));
- (id<Weather_sharedKoin_coreKoinExtension> _Nullable)getExtensionOrNullId:(NSString *)id __attribute__((swift_name("getExtensionOrNull(id:)")));
- (void)registerExtensionId:(NSString *)id extension:(id<Weather_sharedKoin_coreKoinExtension>)extension __attribute__((swift_name("registerExtension(id:extension:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_coreInstanceRegistry")))
@interface Weather_sharedKoin_coreInstanceRegistry : Weather_sharedBase
- (instancetype)initWith_koin:(Weather_sharedKoin_coreKoin *)_koin __attribute__((swift_name("init(_koin:)"))) __attribute__((objc_designated_initializer));
- (void)saveMappingAllowOverride:(BOOL)allowOverride mapping:(NSString *)mapping factory:(Weather_sharedKoin_coreInstanceFactory<id> *)factory logWarning:(BOOL)logWarning __attribute__((swift_name("saveMapping(allowOverride:mapping:factory:logWarning:)")));
- (int32_t)size __attribute__((swift_name("size()")));
@property (readonly) Weather_sharedKoin_coreKoin *_koin __attribute__((swift_name("_koin")));
@property (readonly) NSDictionary<NSString *, Weather_sharedKoin_coreInstanceFactory<id> *> *instances __attribute__((swift_name("instances")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_corePropertyRegistry")))
@interface Weather_sharedKoin_corePropertyRegistry : Weather_sharedBase
- (instancetype)initWith_koin:(Weather_sharedKoin_coreKoin *)_koin __attribute__((swift_name("init(_koin:)"))) __attribute__((objc_designated_initializer));
- (void)close __attribute__((swift_name("close()")));
- (void)deletePropertyKey:(NSString *)key __attribute__((swift_name("deleteProperty(key:)")));
- (id _Nullable)getPropertyKey:(NSString *)key __attribute__((swift_name("getProperty(key:)")));
- (void)savePropertiesProperties:(NSDictionary<NSString *, id> *)properties __attribute__((swift_name("saveProperties(properties:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_coreScopeRegistry")))
@interface Weather_sharedKoin_coreScopeRegistry : Weather_sharedBase
- (instancetype)initWith_koin:(Weather_sharedKoin_coreKoin *)_koin __attribute__((swift_name("init(_koin:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Weather_sharedKoin_coreScopeRegistryCompanion *companion __attribute__((swift_name("companion")));
- (void)loadScopesModules:(NSSet<Weather_sharedKoin_coreModule *> *)modules __attribute__((swift_name("loadScopes(modules:)")));
@property (readonly) Weather_sharedKoin_coreScope *rootScope __attribute__((swift_name("rootScope")));
@property (readonly) NSSet<id<Weather_sharedKoin_coreQualifier>> *scopeDefinitions __attribute__((swift_name("scopeDefinitions")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinEnumCompanion")))
@interface Weather_sharedKotlinEnumCompanion : Weather_sharedBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Weather_sharedKotlinEnumCompanion *shared __attribute__((swift_name("shared")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_coreLevel")))
@interface Weather_sharedKoin_coreLevel : Weather_sharedKotlinEnum<Weather_sharedKoin_coreLevel *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) Weather_sharedKoin_coreLevel *debug __attribute__((swift_name("debug")));
@property (class, readonly) Weather_sharedKoin_coreLevel *info __attribute__((swift_name("info")));
@property (class, readonly) Weather_sharedKoin_coreLevel *warning __attribute__((swift_name("warning")));
@property (class, readonly) Weather_sharedKoin_coreLevel *error __attribute__((swift_name("error")));
@property (class, readonly) Weather_sharedKoin_coreLevel *none __attribute__((swift_name("none")));
+ (Weather_sharedKotlinArray<Weather_sharedKoin_coreLevel *> *)values __attribute__((swift_name("values()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_coreKind")))
@interface Weather_sharedKoin_coreKind : Weather_sharedKotlinEnum<Weather_sharedKoin_coreKind *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) Weather_sharedKoin_coreKind *singleton __attribute__((swift_name("singleton")));
@property (class, readonly) Weather_sharedKoin_coreKind *factory __attribute__((swift_name("factory")));
@property (class, readonly) Weather_sharedKoin_coreKind *scoped __attribute__((swift_name("scoped")));
+ (Weather_sharedKotlinArray<Weather_sharedKoin_coreKind *> *)values __attribute__((swift_name("values()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_coreCallbacks")))
@interface Weather_sharedKoin_coreCallbacks<T> : Weather_sharedBase
- (instancetype)initWithOnClose:(void (^ _Nullable)(T _Nullable))onClose __attribute__((swift_name("init(onClose:)"))) __attribute__((objc_designated_initializer));
- (Weather_sharedKoin_coreCallbacks<T> *)doCopyOnClose:(void (^ _Nullable)(T _Nullable))onClose __attribute__((swift_name("doCopy(onClose:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) void (^ _Nullable onClose)(T _Nullable) __attribute__((swift_name("onClose")));
@end

__attribute__((swift_name("Library_baseRealmProperty")))
@protocol Weather_sharedLibrary_baseRealmProperty
@required
@property (readonly) BOOL isNullable __attribute__((swift_name("isNullable")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) id<Weather_sharedLibrary_baseRealmPropertyType> type __attribute__((swift_name("type")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Library_baseRealmClassKind")))
@interface Weather_sharedLibrary_baseRealmClassKind : Weather_sharedKotlinEnum<Weather_sharedLibrary_baseRealmClassKind *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) Weather_sharedLibrary_baseRealmClassKind *standard __attribute__((swift_name("standard")));
@property (class, readonly) Weather_sharedLibrary_baseRealmClassKind *embedded __attribute__((swift_name("embedded")));
@property (class, readonly) Weather_sharedLibrary_baseRealmClassKind *asymmetric __attribute__((swift_name("asymmetric")));
+ (Weather_sharedKotlinArray<Weather_sharedLibrary_baseRealmClassKind *> *)values __attribute__((swift_name("values()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_serialization_jsonJsonElement.Companion")))
@interface Weather_sharedKotlinx_serialization_jsonJsonElementCompanion : Weather_sharedBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Weather_sharedKotlinx_serialization_jsonJsonElementCompanion *shared __attribute__((swift_name("shared")));
- (id<Weather_sharedKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
__attribute__((swift_name("Kotlinx_serialization_jsonJsonNamingStrategy")))
@protocol Weather_sharedKotlinx_serialization_jsonJsonNamingStrategy
@required
- (NSString *)serialNameForJsonDescriptor:(id<Weather_sharedKotlinx_serialization_coreSerialDescriptor>)descriptor elementIndex:(int32_t)elementIndex serialName:(NSString *)serialName __attribute__((swift_name("serialNameForJson(descriptor:elementIndex:serialName:)")));
@end

__attribute__((swift_name("Koin_coreKoinExtension")))
@protocol Weather_sharedKoin_coreKoinExtension
@required
- (void)onClose __attribute__((swift_name("onClose()")));
@property Weather_sharedKoin_coreKoin *koin __attribute__((swift_name("koin")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_coreScopeRegistry.Companion")))
@interface Weather_sharedKoin_coreScopeRegistryCompanion : Weather_sharedBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Weather_sharedKoin_coreScopeRegistryCompanion *shared __attribute__((swift_name("shared")));
@end

__attribute__((swift_name("Library_baseRealmPropertyType")))
@protocol Weather_sharedLibrary_baseRealmPropertyType
@required
@property (readonly) BOOL isNullable __attribute__((swift_name("isNullable")));
@property (readonly) Weather_sharedLibrary_baseRealmStorageType *storageType __attribute__((swift_name("storageType")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Library_baseRealmStorageType")))
@interface Weather_sharedLibrary_baseRealmStorageType : Weather_sharedKotlinEnum<Weather_sharedLibrary_baseRealmStorageType *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) Weather_sharedLibrary_baseRealmStorageType *bool_ __attribute__((swift_name("bool_")));
@property (class, readonly) Weather_sharedLibrary_baseRealmStorageType *int_ __attribute__((swift_name("int_")));
@property (class, readonly) Weather_sharedLibrary_baseRealmStorageType *string __attribute__((swift_name("string")));
@property (class, readonly) Weather_sharedLibrary_baseRealmStorageType *binary __attribute__((swift_name("binary")));
@property (class, readonly) Weather_sharedLibrary_baseRealmStorageType *object __attribute__((swift_name("object")));
@property (class, readonly) Weather_sharedLibrary_baseRealmStorageType *float_ __attribute__((swift_name("float_")));
@property (class, readonly) Weather_sharedLibrary_baseRealmStorageType *double_ __attribute__((swift_name("double_")));
@property (class, readonly) Weather_sharedLibrary_baseRealmStorageType *decimal128 __attribute__((swift_name("decimal128")));
@property (class, readonly) Weather_sharedLibrary_baseRealmStorageType *timestamp __attribute__((swift_name("timestamp")));
@property (class, readonly) Weather_sharedLibrary_baseRealmStorageType *objectId __attribute__((swift_name("objectId")));
@property (class, readonly) Weather_sharedLibrary_baseRealmStorageType *uuid __attribute__((swift_name("uuid")));
@property (class, readonly) Weather_sharedLibrary_baseRealmStorageType *any __attribute__((swift_name("any")));
+ (Weather_sharedKotlinArray<Weather_sharedLibrary_baseRealmStorageType *> *)values __attribute__((swift_name("values()")));
@property (readonly) id<Weather_sharedKotlinKClass> kClass __attribute__((swift_name("kClass")));
@end

#pragma pop_macro("_Nullable_result")
#pragma clang diagnostic pop
NS_ASSUME_NONNULL_END
