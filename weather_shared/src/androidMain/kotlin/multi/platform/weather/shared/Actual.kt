package multi.platform.weather.shared

import io.ktor.client.engine.HttpClientEngine
import io.ktor.client.engine.okhttp.OkHttp
import multi.platform.weather.shared.app.common.ItemWeatherForecastViewModel
import multi.platform.weather.shared.app.searchbar.ListWeatherLocationViewModel
import multi.platform.weather.shared.data.weather.WeatherRepositoryImpl
import multi.platform.weather.shared.domain.weather.WeatherRepository
import multi.platform.weather.shared.domain.weather.usecase.GetWeatherForecastLocalUseCase
import multi.platform.weather.shared.domain.weather.usecase.GetWeatherForecastNetworkUseCase
import multi.platform.weather.shared.domain.weather.usecase.GetWeatherLocationsLocalUseCase
import multi.platform.weather.shared.domain.weather.usecase.GetWeatherLocationsNetworkUseCase
import multi.platform.weather.shared.domain.weather.usecase.SetWeatherForecastLocalUseCase
import multi.platform.weather.shared.domain.weather.usecase.SetWeatherLocationsLocalUseCase
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module

actual fun weatherModule() = module {
    single<HttpClientEngine> { OkHttp.create() }
    single<WeatherRepository> { WeatherRepositoryImpl(get(), get(), get()) }

    singleOf(::GetWeatherForecastLocalUseCase)
    singleOf(::GetWeatherForecastNetworkUseCase)
    singleOf(::GetWeatherLocationsLocalUseCase)
    singleOf(::GetWeatherLocationsNetworkUseCase)
    singleOf(::SetWeatherForecastLocalUseCase)
    singleOf(::SetWeatherLocationsLocalUseCase)

    viewModelOf(::ListWeatherLocationViewModel)
    viewModelOf(::ItemWeatherForecastViewModel)
}
