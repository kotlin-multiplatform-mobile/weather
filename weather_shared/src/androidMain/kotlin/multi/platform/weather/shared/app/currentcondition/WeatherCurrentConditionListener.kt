package multi.platform.weather.shared.app.currentcondition

import multi.platform.weather.shared.domain.weather.entity.WeatherCondition

interface WeatherCurrentConditionListener {
    fun onDataChanged(weatherCondition: WeatherCondition)
}
