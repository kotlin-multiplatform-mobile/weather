package multi.platform.weather.shared.app.searchbar

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.ArrayAdapter
import androidx.core.widget.addTextChangedListener
import androidx.core.widget.doOnTextChanged
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Lifecycle
import androidx.preference.PreferenceManager
import kotlinx.coroutines.Job
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import multi.platform.core.shared.app.common.CoreFragment
import multi.platform.core.shared.external.extensions.hideKeyboard
import multi.platform.core.shared.external.extensions.launchAndCollectIn
import multi.platform.weather.shared.R
import multi.platform.weather.shared.databinding.WeatherSearchbarFragmentBinding
import multi.platform.weather.shared.external.constants.WeatherKey
import org.koin.androidx.viewmodel.ext.android.viewModel

class WeatherSearchbarFragment : CoreFragment(), WeatherSearchbarListener {
    private val listWeatherLocationViewModel: ListWeatherLocationViewModel by viewModel()
    private lateinit var binding: WeatherSearchbarFragmentBinding
    lateinit var autoComplete: ArrayAdapter<String>
    private var weatherSearchbarListener: WeatherSearchbarListener? = null
    private var searchJob: Job? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        weatherSearchbarListener = parentFragment as? WeatherSearchbarListener
        binding =
            DataBindingUtil.inflate(inflater, R.layout.weather_searchbar_fragment, container, false)
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        listWeatherLocationViewModel.apply {
            isFromNetwork = false
            load()
            isFromNetwork = true
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            lifecycleOwner = viewLifecycleOwner
            weatherLocationsVM = listWeatherLocationViewModel.also {
                it.search =
                    PreferenceManager.getDefaultSharedPreferences(requireContext()).getString(
                        WeatherKey.QUERY,
                        "Jakarta",
                    )
                it.items.launchAndCollectIn(
                    this@WeatherSearchbarFragment,
                    Lifecycle.State.STARTED,
                ) { items ->
                    items?.let { its ->
                        autoComplete.clear()
                        autoComplete.addAll(its.map { it.name }.distinct())
                        autoComplete.notifyDataSetChanged()
                    }
                }
            }
            autoComplete =
                ArrayAdapter(requireContext(), android.R.layout.simple_dropdown_item_1line)
            actSearchLocation.apply {
                clearFocus()
                setAdapter(autoComplete)
                setText(listWeatherLocationViewModel.search.toString())
                setOnItemClickListener { _, _, position, _ ->
                    listWeatherLocationViewModel.search = autoComplete.getItem(position) ?: ""
                    weatherSearchbarListener?.onWeatherSearch(listWeatherLocationViewModel.search.toString())
                    hideKeyboard()
                    clearFocus()
                }
                addTextChangedListener {
                    doOnTextChanged { text, _, _, _ ->
                        text?.takeIf { it.isNotEmpty() && it.isNotBlank() }?.let {
                            searchJob?.cancel()
                            searchJob = MainScope().launch {
                                delay(300)
                                listWeatherLocationViewModel.search = it.toString()
                                listWeatherLocationViewModel.load()
                                PreferenceManager.getDefaultSharedPreferences(requireContext())
                                    .edit()
                                    .putString(WeatherKey.QUERY, it.toString())
                                    .apply()
                            }
                        }
                    }
                }
                setOnEditorActionListener { textView, actionId, _ ->
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        hideKeyboard()
                        clearFocus()
                        searchJob?.cancel()
                        listWeatherLocationViewModel.search = textView.text.toString()
                        weatherSearchbarListener?.onWeatherSearch(
                            listWeatherLocationViewModel.search.toString(),
                        )

                        return@setOnEditorActionListener true
                    }
                    false
                }
            }
        }
    }

    override fun refreshWeatherSearch() {
        listWeatherLocationViewModel.search =
            PreferenceManager.getDefaultSharedPreferences(requireContext()).getString(
                WeatherKey.QUERY,
                "Jakarta",
            )
        binding.actSearchLocation.setText(listWeatherLocationViewModel.search.toString())
    }
}
