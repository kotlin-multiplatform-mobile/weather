package multi.platform.weather.shared.app.currentcondition

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import multi.platform.core.shared.app.common.CoreFragment
import multi.platform.core.shared.external.extensions.loadImage
import multi.platform.weather.shared.R
import multi.platform.weather.shared.databinding.WeatherCurrentConditionFragmentBinding
import multi.platform.weather.shared.domain.weather.entity.WeatherCondition

class WeatherCurrentConditionFragment : CoreFragment(), WeatherCurrentConditionListener {
    private lateinit var binding: WeatherCurrentConditionFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.weather_current_condition_fragment, container, false)
        return binding.root
    }

    override fun onDataChanged(weatherCondition: WeatherCondition) {
        binding.apply {
            tvWeatherCondition.text = weatherCondition.text.toString()
            ivWeatherCondition.loadImage("https:" + weatherCondition.icon.toString())
        }
    }
}
