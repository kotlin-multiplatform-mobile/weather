package multi.platform.weather.shared.app.currenttemp

interface WeatherCurrentTempListener {
    fun onDataChanged(localTime: String, location: String, temp: Double)
}
