package multi.platform.weather.shared.app.forecast

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import io.ktor.util.toLowerCasePreservingASCIIRules
import multi.platform.core.shared.DecimalFormat
import multi.platform.core.shared.app.common.CoreFragment
import multi.platform.core.shared.external.extensions.loadImage
import multi.platform.core.shared.formatDate
import multi.platform.weather.shared.R
import multi.platform.weather.shared.databinding.WeatherConditionItemBinding
import multi.platform.weather.shared.databinding.WeatherForecastFragmentBinding
import multi.platform.weather.shared.databinding.WeatherForecastItemBinding
import multi.platform.weather.shared.domain.weather.entity.Weather
import kotlin.math.abs

class WeatherForecastFragment : CoreFragment(), WeatherForecastListener {
    private lateinit var binding: WeatherForecastFragmentBinding
    private var sourceDateFormat = "yyyy-MM-dd HH:mm"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.weather_forecast_fragment, container, false)
        return binding.root
    }

    override fun onDataChanged(weathers: List<Weather>) {
        binding.apply {
            rcvWeatherForecast.apply {
                val closest = weathers.filter { it.timeEpoch != null }
                    .minBy { abs((System.currentTimeMillis() / 1000) - it.timeEpoch!!) }
                adapter = AdapterForecast(weathers.toList())
                layoutManager?.scrollToPosition(weathers.indexOf(closest))
                showDetail(closest)
            }
        }
    }

    inner class AdapterForecast(val data: List<Weather>) :
        RecyclerView.Adapter<AdapterForecast.ViewHolder>() {
        inner class ViewHolder(view: View, val itemBinding: WeatherForecastItemBinding) :
            RecyclerView.ViewHolder(view) {
            @SuppressLint("SetTextI18n")
            fun bind(weather: Weather) {
                val temp = DecimalFormat().format(weather.tempC, 0) + "°"
                itemBinding.tvWeatherForecastTemp.text = temp
                itemBinding.tvWeatherForecastTime.text =
                    formatDate(weather.time.toString(), sourceDateFormat, "ha").toLowerCasePreservingASCIIRules()
                itemBinding.ivWeatherForecastIcon.loadImage("https:" + weather.condition?.icon)
                itemBinding.root.setOnClickListener {
                    showDetail(weather)
                }
            }
        }

        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int,
        ): AdapterForecast.ViewHolder {
            val binding = DataBindingUtil.inflate<WeatherForecastItemBinding>(
                LayoutInflater.from(parent.context),
                R.layout.weather_forecast_item,
                parent,
                false,
            )
            return ViewHolder(binding.root, binding)
        }

        override fun getItemCount() = data.size
        override fun onBindViewHolder(holder: AdapterForecast.ViewHolder, position: Int) {
            holder.bind(data[position])
        }
    }

    private fun showDetail(weather: Weather) {
        val temp = DecimalFormat().format(weather.tempC, 0) + "°"
        val time = formatDate(weather.time.toString(), sourceDateFormat, "hh:mm a").toLowerCasePreservingASCIIRules()
        binding.tvWeatherConditionTitle.text = getString(R.string.weather_condition_title, time)
        binding.rcvWeatherCondition.apply {
            adapter = AdapterCondition(
                listOf(
                    ConditionItem("Temp", temp),
                    ConditionItem("Wind Dir", weather.windDir.toString()),
                    ConditionItem("Humidity", weather.humidity.toString()),
                    ConditionItem("UV", weather.uv.toString()),
                ),
            )
        }
    }

    inner class ConditionItem(val name: String, val condition: String)

    inner class AdapterCondition(val data: List<ConditionItem>) :
        RecyclerView.Adapter<AdapterCondition.ViewHolder>() {
        inner class ViewHolder(view: View, val itemBinding: WeatherConditionItemBinding) :
            RecyclerView.ViewHolder(view) {
            fun bind(conditionItem: ConditionItem) {
                itemBinding.tvWeatherConditionName.text = conditionItem.name
                itemBinding.tvWeatherCondition.text = conditionItem.condition
            }
        }

        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int,
        ): AdapterCondition.ViewHolder {
            val binding = DataBindingUtil.inflate<WeatherConditionItemBinding>(
                LayoutInflater.from(parent.context),
                R.layout.weather_condition_item,
                parent,
                false,
            )
            return ViewHolder(binding.root, binding)
        }

        override fun getItemCount() = 4
        override fun onBindViewHolder(holder: AdapterCondition.ViewHolder, position: Int) {
            holder.bind(data[position])
        }
    }
}
