package multi.platform.weather.shared.app.searchbar

interface WeatherSearchbarListener {
    fun onWeatherSearch(query: String) = Unit
    fun refreshWeatherSearch() = Unit
}
