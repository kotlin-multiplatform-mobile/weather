package multi.platform.weather.shared.app.currenttemp

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import multi.platform.core.shared.DecimalFormat
import multi.platform.core.shared.app.common.CoreFragment
import multi.platform.core.shared.formatDate
import multi.platform.weather.shared.R
import multi.platform.weather.shared.databinding.WeatherCurrentTempFragmentBinding

class WeatherCurrentTempFragment : CoreFragment(), WeatherCurrentTempListener {
    private lateinit var binding: WeatherCurrentTempFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.weather_current_temp_fragment, container, false)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onDataChanged(localTime: String, location: String, temp: Double) {
        binding.apply {
            tvWeatherDate.text = formatDate(
                localTime,
                "yyyy-MM-dd HH:mm",
                "d MMMM YYYY",
            )
            tvWeatherLocation.text = location
            tvWeatherTemp.text = DecimalFormat().format(temp, 0) + "°"
        }
    }
}
