package multi.platform.weather.shared.app.astronomy

import multi.platform.weather.shared.domain.weather.entity.WeatherAstronomy

interface WeatherAstronomyListener {
    fun onDataChanged(weatherAstronomy: WeatherAstronomy)
}
