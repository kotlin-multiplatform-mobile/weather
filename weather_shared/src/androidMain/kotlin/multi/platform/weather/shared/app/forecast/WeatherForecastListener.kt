package multi.platform.weather.shared.app.forecast

import multi.platform.weather.shared.domain.weather.entity.Weather

interface WeatherForecastListener {
    fun onDataChanged(weathers: List<Weather>)
}
