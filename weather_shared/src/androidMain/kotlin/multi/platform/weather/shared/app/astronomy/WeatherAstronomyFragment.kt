package multi.platform.weather.shared.app.astronomy

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import multi.platform.core.shared.app.common.CoreFragment
import multi.platform.weather.shared.R
import multi.platform.weather.shared.databinding.WeatherAstronomyFragmentBinding
import multi.platform.weather.shared.databinding.WeatherAstronomyItemBinding
import multi.platform.weather.shared.domain.weather.entity.WeatherAstronomy

class WeatherAstronomyFragment : CoreFragment(), WeatherAstronomyListener {
    private lateinit var binding: WeatherAstronomyFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.weather_astronomy_fragment, container, false)
        return binding.root
    }

    override fun onDataChanged(weatherAstronomy: WeatherAstronomy) {
        binding.apply {
            rcvWeatherAstronomy.apply {
                adapter = Adapter(
                    listOf(
                        ConditionItem("Sunrise", weatherAstronomy.sunrise.toString()),
                        ConditionItem("Sunset", weatherAstronomy.sunset.toString()),
                        ConditionItem("Moonrise", weatherAstronomy.moonrise.toString()),
                        ConditionItem("Moonset", weatherAstronomy.moonset.toString()),
                    ),
                )
            }
        }
    }

    inner class ConditionItem(val name: String, val time: String)

    inner class Adapter(val data: List<ConditionItem>) : RecyclerView.Adapter<Adapter.ViewHolder>() {
        inner class ViewHolder(view: View, val binding: WeatherAstronomyItemBinding) : RecyclerView.ViewHolder(view) {
            fun bind(conditionItem: ConditionItem) {
                binding.tvWeatherAstronomyName.text = conditionItem.name
                binding.tvWeatherAstronomyTime.text = conditionItem.time
            }
        }
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Adapter.ViewHolder {
            val binding = DataBindingUtil.inflate<WeatherAstronomyItemBinding>(LayoutInflater.from(parent.context), R.layout.weather_astronomy_item, parent, false)
            return ViewHolder(binding.root, binding)
        }
        override fun getItemCount() = 4
        override fun onBindViewHolder(holder: Adapter.ViewHolder, position: Int) {
            holder.bind(data[position])
        }
    }
}
