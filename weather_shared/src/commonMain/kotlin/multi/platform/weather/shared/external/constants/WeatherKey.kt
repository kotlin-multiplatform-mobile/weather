package multi.platform.weather.shared.external.constants

object WeatherKey {
    const val NIGHT_MODE = "night-mode"
    const val QUERY = "query"
}
