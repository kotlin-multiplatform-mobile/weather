@file:Suppress("UNUSED")

package multi.platform.weather.shared.domain.weather.entity

import io.realm.kotlin.types.RealmObject
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
open class Weather : RealmObject {
    @SerialName("time_epoch")
    var timeEpoch: Long? = null
    var time: String? = null

    @SerialName("is_day")
    var isDay: Int? = null

    @SerialName("temp_c")
    var tempC: Double = 0.0

    @SerialName("temp_f")
    var tempF: Double = 0.0
    var condition: WeatherCondition? = null

    @SerialName("wind_mph")
    var windMph: Double = 0.0

    @SerialName("wind_kph")
    var windKph: Double = 0.0

    @SerialName("wind_degree")
    var windDegree: Double = 0.0

    @SerialName("wind_dir")
    var windDir: String? = null

    @SerialName("pressure_mb")
    var pressureMb: Double = 0.0

    @SerialName("pressure_in")
    var pressureIn: Double = 0.0

    @SerialName("precip_mm")
    var precipMm: Double = 0.0

    @SerialName("precip_in")
    var precipIn: Double = 0.0
    var humidity: Double = 0.0
    var cloud: Double = 0.0

    @SerialName("feelslike_c")
    var feelslikeC: Double = 0.0

    @SerialName("feelslike_f")
    var feelslikeF: Double = 0.0

    @SerialName("windchill_c")
    var windchillC: Double? = null

    @SerialName("windchill_f")
    var windchillF: Double? = null

    @SerialName("heatindex_c")
    var heatindexC: Double? = null

    @SerialName("heatindex_f")
    var heatindexF: Double? = null

    @SerialName("dewpoint_c")
    var dewpointC: Double? = null

    @SerialName("dewpoint_f")
    var dewpointF: Double? = null

    @SerialName("will_it_rain")
    var willItRain: Int? = null

    @SerialName("chance_of_rain")
    var chanceOfRain: Double? = null

    @SerialName("will_it_snow")
    var willItSnow: Int? = null

    @SerialName("chance_of_snow")
    var chanceOfSnow: Int? = null

    @SerialName("vis_km")
    var visKm: Double = 0.0

    @SerialName("vis_miles")
    var visMiles: Double = 0.0

    @SerialName("gust_mph")
    var gustMph: Double = 0.0

    @SerialName("gust_kph")
    var gustKph: Double = 0.0
    var uv: Double = 0.0
}
