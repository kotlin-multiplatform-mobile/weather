package multi.platform.weather.shared

import org.koin.core.module.Module

expect fun weatherModule(): Module
