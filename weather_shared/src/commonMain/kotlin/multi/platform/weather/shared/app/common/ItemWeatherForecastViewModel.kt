@file:Suppress("UNCHECKED_CAST")

package multi.platform.weather.shared.app.common

import kotlinx.datetime.Clock
import multi.platform.core.shared.app.common.ItemViewModel
import multi.platform.core.shared.external.extensions.removeRange
import multi.platform.weather.shared.domain.weather.entity.Weather
import multi.platform.weather.shared.domain.weather.entity.WeatherAstronomy
import multi.platform.weather.shared.domain.weather.entity.WeatherForecast
import multi.platform.weather.shared.domain.weather.entity.WeatherForecastday
import multi.platform.weather.shared.domain.weather.usecase.GetWeatherForecastLocalUseCase
import multi.platform.weather.shared.domain.weather.usecase.GetWeatherForecastNetworkUseCase
import multi.platform.weather.shared.domain.weather.usecase.SetWeatherForecastLocalUseCase
import kotlin.math.abs

class ItemWeatherForecastViewModel(
    getUseCase: GetWeatherForecastNetworkUseCase,
    readUseCase: GetWeatherForecastLocalUseCase,
    setUseCase: SetWeatherForecastLocalUseCase,
) : ItemViewModel<
    WeatherForecast,
    GetWeatherForecastNetworkUseCase,
    GetWeatherForecastLocalUseCase,
    SetWeatherForecastLocalUseCase,
    >(
    getUseCase,
    readUseCase,
    setUseCase,
) {
    fun prepareWeathers(
        weatherForecastdays: List<WeatherForecastday>,
        currentEpoch: Long = Clock.System.now().epochSeconds,
    ): HashMap<String, Any> {
        val weathers = mutableListOf<Weather>()
        weatherForecastdays.forEach { it.hour?.let { h -> weathers.addAll(h.toList()) } }
        val closest = weathers.filter { it.timeEpoch != null }
            .minBy { abs(currentEpoch - it.timeEpoch!!) }
        val map = hashMapOf<String, Any>()
        map["closest"] = closest
        map["weathers"] = weathers
        return map
    }

    fun prepareAstronomy(
        weatherForecastdays: List<WeatherForecastday>,
        currentEpoch: Long = Clock.System.now().epochSeconds,
    ): WeatherAstronomy? {
        var astro = weatherForecastdays[0].astro
        val map = prepareWeathers(weatherForecastdays, currentEpoch)
        val weathers = map["weathers"] as MutableList<Weather>
        if (weathers.indexOf(map["closest"]) == weathers.size / 2) {
            astro = weatherForecastdays[1].astro
        }
        return astro
    }

    fun filterWeathers(
        weatherForecastdays: List<WeatherForecastday>,
        currentEpoch: Long = Clock.System.now().epochSeconds,
    ): List<Weather> {
        val map = prepareWeathers(weatherForecastdays, currentEpoch)
        val weathers = map["weathers"] as MutableList<Weather>
        val divider = weathers.size / 2
        if (weathers.indexOf(map["closest"]) == divider) {
            weathers.removeRange(IntRange(0, (weathers.size / 4) - 1))
            weathers.removeRange(IntRange(divider, weathers.size - 1))
        } else weathers.removeRange(IntRange(divider, weathers.size - 1))
        return weathers
    }
}
