@file:Suppress("UNCHECKED_CAST")

package multi.platform.weather.shared.domain.weather.usecase

import multi.platform.core.shared.domain.common.usecase.CoreUseCase
import multi.platform.weather.shared.domain.weather.WeatherRepository
import multi.platform.weather.shared.domain.weather.entity.WeatherForecast

class SetWeatherForecastLocalUseCase(
    private val weatherRepository: WeatherRepository,
) : CoreUseCase {
    override suspend fun call(vararg args: Any?): WeatherForecast =
        weatherRepository.setWeatherForecastLocal(args[0] as WeatherForecast)

    override suspend fun onError(e: Exception) {
        throw e
    }
}
