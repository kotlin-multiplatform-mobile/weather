package multi.platform.weather.shared.domain.weather

import multi.platform.weather.shared.domain.weather.entity.WeatherForecast
import multi.platform.weather.shared.domain.weather.entity.WeatherLocation

interface WeatherRepository {
    suspend fun getWeatherLocationsNetwork(
        offset: Int,
        limit: Int,
        search: String?,
        order: String?,
    ): List<WeatherLocation>
    suspend fun setWeatherLocationsLocal(weatherLocations: List<WeatherLocation>): Boolean
    suspend fun getWeatherLocationsLocal(
        offset: Int,
        limit: Int,
        search: String?,
    ): List<WeatherLocation>

    suspend fun getWeatherForecastNetwork(query: String): WeatherForecast
    suspend fun setWeatherForecastLocal(weatherForecast: WeatherForecast): WeatherForecast
    suspend fun getWeatherForecastLocal(search: String?): WeatherForecast
}
