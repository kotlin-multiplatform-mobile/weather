package multi.platform.weather.shared.domain.weather.entity

import io.realm.kotlin.types.RealmObject
import kotlinx.serialization.Serializable

@Serializable
open class WeatherCondition : RealmObject {
    var text: String? = null
    var icon: String? = null
    var code: Int? = null
}
