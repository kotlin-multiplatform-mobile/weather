package multi.platform.weather.shared.app.searchbar

import multi.platform.core.shared.app.common.ListViewModel
import multi.platform.weather.shared.domain.weather.entity.WeatherLocation
import multi.platform.weather.shared.domain.weather.usecase.GetWeatherLocationsLocalUseCase
import multi.platform.weather.shared.domain.weather.usecase.GetWeatherLocationsNetworkUseCase
import multi.platform.weather.shared.domain.weather.usecase.SetWeatherLocationsLocalUseCase

class ListWeatherLocationViewModel(
    getUseCase: GetWeatherLocationsNetworkUseCase,
    readUseCase: GetWeatherLocationsLocalUseCase,
    setUseCase: SetWeatherLocationsLocalUseCase,
) : ListViewModel<
    WeatherLocation,
    List<WeatherLocation>,
    GetWeatherLocationsNetworkUseCase,
    GetWeatherLocationsLocalUseCase,
    SetWeatherLocationsLocalUseCase,
    >(
    getUseCase,
    readUseCase,
    setUseCase,
)
