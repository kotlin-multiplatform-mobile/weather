package multi.platform.weather.shared.data.weather

import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import io.ktor.client.request.host
import io.realm.kotlin.Realm
import io.realm.kotlin.ext.query
import multi.platform.core.shared.external.utilities.network.ApiClientProvider
import multi.platform.weather.shared.domain.weather.WeatherRepository
import multi.platform.weather.shared.domain.weather.entity.WeatherForecast
import multi.platform.weather.shared.domain.weather.entity.WeatherLocation
import multi.platform.weather.shared.external.WeatherConfig

class WeatherRepositoryImpl(
    private val realm: Realm,
    private val weatherConfig: WeatherConfig,
    private val apiClientProvider: ApiClientProvider<HttpClient>,
) : WeatherRepository {
    override suspend fun getWeatherLocationsNetwork(
        offset: Int,
        limit: Int,
        search: String?,
        order: String?,
    ): List<WeatherLocation> =
        apiClientProvider.client.get("/v1/search.json?q=$search") {
            host = weatherConfig.host
            headers.append("key", weatherConfig.apiKey)
        }.body()

    override suspend fun setWeatherLocationsLocal(weatherLocations: List<WeatherLocation>) = realm.write {
        for (weatherLocation in weatherLocations) copyToRealm(weatherLocation)
        true
    }

    override suspend fun getWeatherLocationsLocal(
        offset: Int,
        limit: Int,
        search: String?,
    ): List<WeatherLocation> = realm.query<WeatherLocation>().limit(limit).find().toMutableList()

    override suspend fun getWeatherForecastNetwork(query: String): WeatherForecast =
        apiClientProvider.client.get("/v1/forecast.json?q=$query&days=2&aqi=no&alerts=no") {
            host = weatherConfig.host
            headers.append("key", weatherConfig.apiKey)
        }.body()

    override suspend fun setWeatherForecastLocal(weatherForecast: WeatherForecast) = realm.write {
        copyToRealm(weatherForecast)
    }

    override suspend fun getWeatherForecastLocal(search: String?): WeatherForecast =
        realm.query<WeatherForecast>("location.name = $1", search).find().first()
}
