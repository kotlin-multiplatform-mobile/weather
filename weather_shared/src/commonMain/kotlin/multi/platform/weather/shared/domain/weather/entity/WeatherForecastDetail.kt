package multi.platform.weather.shared.domain.weather.entity

import io.realm.kotlin.types.RealmList
import io.realm.kotlin.types.RealmObject
import kotlinx.serialization.Serializable
import multi.platform.weather.shared.external.realm.RealmListWeatherForecastdaySerializer

@Serializable
open class WeatherForecastDetail : RealmObject {
    @Serializable(with = RealmListWeatherForecastdaySerializer::class)
    var forecastday: RealmList<WeatherForecastday>? = null
}
