package multi.platform.weather.shared.domain.weather.entity

import kotlinx.serialization.Serializable

@Serializable
data class WeatherError(
    val error: Error,
) {
    @Serializable
    data class Error(
        val code: String?,
        val message: String?,
    )
}
