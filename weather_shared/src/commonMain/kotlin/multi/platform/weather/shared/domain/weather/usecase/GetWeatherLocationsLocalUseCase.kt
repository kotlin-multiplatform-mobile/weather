package multi.platform.weather.shared.domain.weather.usecase

import multi.platform.core.shared.domain.common.usecase.CoreUseCase
import multi.platform.weather.shared.domain.weather.WeatherRepository
import multi.platform.weather.shared.domain.weather.entity.WeatherLocation

class GetWeatherLocationsLocalUseCase(
    private val weatherRepository: WeatherRepository,
) : CoreUseCase {
    override suspend fun call(vararg args: Any?): List<WeatherLocation> =
        weatherRepository.getWeatherLocationsLocal(
            args[0] as Int,
            args[1] as Int,
            args[2] as String?,
        )

    override suspend fun onError(e: Exception) {
        throw e
    }
}
