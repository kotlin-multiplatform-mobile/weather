package multi.platform.weather.shared.domain.weather.entity

import io.realm.kotlin.types.RealmObject
import kotlinx.serialization.Serializable

@Serializable
open class WeatherForecast : RealmObject {
    var location: WeatherLocation? = null
    var current: Weather? = null
    var forecast: WeatherForecastDetail? = null
}
