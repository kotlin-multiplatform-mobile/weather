package multi.platform.weather.shared.external

interface WeatherConfig {
    val host: String
    val apiKey: String
}
