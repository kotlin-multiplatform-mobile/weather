package multi.platform.weather.shared.domain.weather.usecase

import multi.platform.core.shared.domain.common.usecase.CoreUseCase
import multi.platform.weather.shared.domain.weather.WeatherRepository
import multi.platform.weather.shared.domain.weather.entity.WeatherForecast

class GetWeatherForecastNetworkUseCase(
    private val weatherRepository: WeatherRepository,
) : CoreUseCase {
    override suspend fun call(vararg args: Any?): WeatherForecast =
        weatherRepository.getWeatherForecastNetwork(args[0] as String)

    override suspend fun onError(e: Exception) {
        throw e
    }
}
