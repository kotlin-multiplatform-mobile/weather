package multi.platform.weather.shared

import io.realm.kotlin.Realm
import io.realm.kotlin.RealmConfiguration
import multi.platform.weather.shared.domain.weather.entity.Weather
import multi.platform.weather.shared.domain.weather.entity.WeatherAstronomy
import multi.platform.weather.shared.domain.weather.entity.WeatherCondition
import multi.platform.weather.shared.domain.weather.entity.WeatherForecast
import multi.platform.weather.shared.domain.weather.entity.WeatherForecastDetail
import multi.platform.weather.shared.domain.weather.entity.WeatherForecastday
import multi.platform.weather.shared.domain.weather.entity.WeatherLocation
import multi.platform.weather.shared.domain.weather.entity.Weatherday
import multi.platform.weather.shared.external.WeatherConfig
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module

class WeatherModule(private val weatherConfig: WeatherConfig) {
    operator fun invoke() = module {
        single<WeatherConfig> { weatherConfig }
        singleOf(::provideDB)
        includes(weatherModule())
    }

    val realmConfig = RealmConfiguration.Builder(
        schema = setOf(
            Weather::class,
            WeatherAstronomy::class,
            WeatherCondition::class,
            Weatherday::class,
            WeatherForecast::class,
            WeatherForecastday::class,
            WeatherForecastDetail::class,
            WeatherLocation::class,
        ),
    )
        .name("weather.realm")
        .schemaVersion(1)
        .deleteRealmIfMigrationNeeded()
        .build()

    fun provideDB() = Realm.open(realmConfig)
}
