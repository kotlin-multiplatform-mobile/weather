package multi.platform.weather.shared.domain.weather.entity

import io.realm.kotlin.types.RealmObject
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
open class WeatherLocation : RealmObject {
    var id: Int? = null
    var name: String? = null
    var region: String? = null
    var country: String? = null
    var lat: Double? = null
    var lon: Double? = null
    var url: String? = null
    var localtime: String? = null

    @SerialName("tz_id")
    var tzId: String? = null

    @SerialName("localtime_epoch")
    var localtimeEpoch: Double? = null
}
