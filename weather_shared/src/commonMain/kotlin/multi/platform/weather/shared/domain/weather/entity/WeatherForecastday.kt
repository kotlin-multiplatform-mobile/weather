package multi.platform.weather.shared.domain.weather.entity

import io.realm.kotlin.types.RealmList
import io.realm.kotlin.types.RealmObject
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import multi.platform.weather.shared.external.realm.RealmListWeatherSerializer

@Serializable
open class WeatherForecastday : RealmObject {
    var date: String? = null

    @SerialName("date_epoch")
    var dateEpoch: Double? = null
    var day: Weatherday? = null
    var astro: WeatherAstronomy? = null

    @Serializable(with = RealmListWeatherSerializer::class)
    var hour: RealmList<Weather>? = null
}
