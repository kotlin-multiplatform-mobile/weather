package multi.platform.weather.shared.domain.weather.entity

import io.realm.kotlin.types.RealmObject
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
open class WeatherAstronomy : RealmObject {
    var sunrise: String? = null
    var sunset: String? = null
    var moonrise: String? = null
    var moonset: String? = null

    @SerialName("moon_phase")
    var moonPhase: String? = null

    @SerialName("moon_illumination")
    var moonIllumination: String? = null

    @SerialName("is_moon_up")
    var isMoonUp: Int? = null

    @SerialName("is_sun_up")
    var isSunUp: Int? = null
}
