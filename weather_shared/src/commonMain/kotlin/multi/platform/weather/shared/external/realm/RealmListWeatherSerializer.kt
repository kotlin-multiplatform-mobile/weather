package multi.platform.weather.shared.external.realm

import io.realm.kotlin.ext.realmListOf
import io.realm.kotlin.types.RealmList
import kotlinx.serialization.KSerializer
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import multi.platform.weather.shared.domain.weather.entity.Weather

class RealmListWeatherSerializer(private val dataSerializer: KSerializer<Weather>) :
    KSerializer<RealmList<Weather>> {
    override fun serialize(encoder: Encoder, value: RealmList<Weather>) {
        encoder.encodeSerializableValue(ListSerializer(dataSerializer), value.toList())
    }
    override fun deserialize(decoder: Decoder): RealmList<Weather> {
        val list = realmListOf<Weather>()
        val items = decoder.decodeSerializableValue(ListSerializer(dataSerializer))
        list.addAll(items)
        return list
    }
    override val descriptor: SerialDescriptor = ListSerializer(dataSerializer).descriptor
}
