package multi.platform.weather.shared.app.searchbar

import io.mockk.clearAllMocks
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import multi.platform.weather.shared.domain.weather.entity.WeatherLocation
import multi.platform.weather.shared.domain.weather.usecase.GetWeatherLocationsLocalUseCase
import multi.platform.weather.shared.domain.weather.usecase.GetWeatherLocationsNetworkUseCase
import multi.platform.weather.shared.domain.weather.usecase.SetWeatherLocationsLocalUseCase
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

@OptIn(ExperimentalCoroutinesApi::class)
class ListWeatherLocationViewModelTest {
    private lateinit var viewModel: ListWeatherLocationViewModel
    private val getUseCase = mockk<GetWeatherLocationsNetworkUseCase>()
    private val readUseCase = mockk<GetWeatherLocationsLocalUseCase>()
    private val setUseCase = mockk<SetWeatherLocationsLocalUseCase>()

    private val page = 0
    private val limit = 1
    private val search = null
    private val order = null

    @BeforeTest
    fun setup() = runTest {
        Dispatchers.setMain(StandardTestDispatcher(testScheduler))
        clearAllMocks()
        viewModel = ListWeatherLocationViewModel(getUseCase, readUseCase, setUseCase)
        viewModel.useAsyncNetworkCall = false
        viewModel.page = page
        viewModel.limit = limit
        viewModel.search = search
        viewModel.order = order
    }

    @Test
    fun `test getFromNetwork should return weather locations from getUseCase`() = runTest {
        // Arrange
        val weatherLocations = mutableListOf(WeatherLocation())
        coEvery { getUseCase.call(limit, page + 1, search, order) } returns weatherLocations
        coEvery { setUseCase.call(weatherLocations) } returns true

        // Act
        viewModel.getFromNetwork()
        advanceUntilIdle()

        // Assert
        assertNotNull(viewModel.items.value)
        assertEquals(weatherLocations, viewModel.items.value)
    }

    @Test
    fun `test getFromLocal should return weather locations from readUseCase`() = runTest {
        // Arrange
        val weatherLocations = mutableListOf(WeatherLocation())
        coEvery { readUseCase.call(limit * page, limit, search, order) } returns weatherLocations

        // Act
        viewModel.getFromLocal()
        advanceUntilIdle()

        // Assert
        assertNotNull(viewModel.items.value)
        assertEquals(weatherLocations, viewModel.items.value)
    }

    @Test
    fun `test saveToLocal should update weather locations`() = runTest {
        // Arrange
        viewModel.items.value = mutableListOf()
        val newWeatherLocations = mutableListOf(WeatherLocation())
        coEvery { setUseCase.call(newWeatherLocations) } returns true

        // Act
        viewModel.saveToLocal(newWeatherLocations)
        advanceUntilIdle()

        // Assert
        assertEquals(newWeatherLocations, viewModel.items.value)
    }
}
