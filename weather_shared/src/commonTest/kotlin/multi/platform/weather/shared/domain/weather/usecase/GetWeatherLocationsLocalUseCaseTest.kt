package multi.platform.weather.shared.domain.weather.usecase

import io.mockk.clearAllMocks
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import multi.platform.weather.shared.domain.weather.WeatherRepository
import multi.platform.weather.shared.domain.weather.entity.WeatherLocation
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

@OptIn(ExperimentalCoroutinesApi::class)
class GetWeatherLocationsLocalUseCaseTest {
    private val weatherRepository = mockk<WeatherRepository>()
    private lateinit var useCase: GetWeatherLocationsLocalUseCase

    @BeforeTest
    fun setup() = runTest {
        Dispatchers.setMain(StandardTestDispatcher(testScheduler))
        clearAllMocks()
        useCase = GetWeatherLocationsLocalUseCase(weatherRepository)
    }

    @Test
    fun `test call should return weather locations`() = runTest {
        // Arrange
        val expectedWeatherLocations = listOf(WeatherLocation())
        coEvery { weatherRepository.getWeatherLocationsLocal(any(), any(), any()) } returns expectedWeatherLocations

        // Act
        val result = useCase.call(1, 2, "New York")

        // Assert
        assertEquals(expectedWeatherLocations, result)
    }

    @Test
    fun `test onError should throw exception`() = runTest {
        // Arrange
        val exception = Exception("An error occurred")

        // Act & Assert
        try {
            useCase.onError(exception)
        } catch (e: Exception) {
            assertEquals(exception, e)
        }
    }
}
