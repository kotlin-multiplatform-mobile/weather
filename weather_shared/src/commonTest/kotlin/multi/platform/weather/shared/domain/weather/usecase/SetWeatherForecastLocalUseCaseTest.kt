package multi.platform.weather.shared.domain.weather.usecase

import io.mockk.clearAllMocks
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import multi.platform.weather.shared.domain.weather.WeatherRepository
import multi.platform.weather.shared.domain.weather.entity.WeatherForecast
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

@OptIn(ExperimentalCoroutinesApi::class)
class SetWeatherForecastLocalUseCaseTest {
    private val weatherRepository = mockk<WeatherRepository>()
    private lateinit var useCase: SetWeatherForecastLocalUseCase

    @BeforeTest
    fun setup() = runTest {
        Dispatchers.setMain(StandardTestDispatcher(testScheduler))
        clearAllMocks()
        useCase = SetWeatherForecastLocalUseCase(weatherRepository)
    }

    @Test
    fun `test call should set weather forecast locally`() = runTest {
        // Arrange
        val weatherForecast = WeatherForecast()
        coEvery { weatherRepository.setWeatherForecastLocal(weatherForecast) } returns weatherForecast

        // Act
        val result = useCase.call(weatherForecast)

        // Assert
        assertEquals(weatherForecast, result)
    }

    @Test
    fun `test onError should throw exception`() = runTest {
        // Arrange
        val exception = Exception("An error occurred")

        // Act & Assert
        try {
            useCase.onError(exception)
        } catch (e: Exception) {
            assertEquals(exception, e)
        }
    }
}
