@file:Suppress("UNCHECKED_CAST")

package multi.platform.weather.shared.app.common

import io.mockk.clearAllMocks
import io.mockk.coEvery
import io.mockk.mockk
import io.realm.kotlin.ext.toRealmList
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import kotlinx.datetime.Clock
import multi.platform.weather.shared.domain.weather.entity.Weather
import multi.platform.weather.shared.domain.weather.entity.WeatherAstronomy
import multi.platform.weather.shared.domain.weather.entity.WeatherForecast
import multi.platform.weather.shared.domain.weather.entity.WeatherForecastday
import multi.platform.weather.shared.domain.weather.usecase.GetWeatherForecastLocalUseCase
import multi.platform.weather.shared.domain.weather.usecase.GetWeatherForecastNetworkUseCase
import multi.platform.weather.shared.domain.weather.usecase.SetWeatherForecastLocalUseCase
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

@OptIn(ExperimentalCoroutinesApi::class)
class ItemWeatherForecastViewModelTest {
    private lateinit var viewModel: ItemWeatherForecastViewModel
    private val getUseCase = mockk<GetWeatherForecastNetworkUseCase>()
    private val readUseCase = mockk<GetWeatherForecastLocalUseCase>()
    private val setUseCase = mockk<SetWeatherForecastLocalUseCase>()
    private val query = "Jakarta"
    private lateinit var weatherForecastdays: List<WeatherForecastday>
    private lateinit var weatherToday: Weather
    private lateinit var weatherTomorrow: Weather
    private val weatherAstronomyToday = WeatherAstronomy().apply { sunrise = "06:00"; sunset = "18:00" }
    private val weatherAstronomyTomorrow = WeatherAstronomy().apply { sunrise = "06:05"; sunset = "18:05" }

    fun hourEpoch(hour: Int) = 60 * 60 * hour

    @BeforeTest
    fun setup() = runTest {
        Dispatchers.setMain(StandardTestDispatcher(testScheduler))
        clearAllMocks()
        viewModel = ItemWeatherForecastViewModel(getUseCase, readUseCase, setUseCase)
        viewModel.useAsyncNetworkCall = false
        weatherToday = Weather().apply { timeEpoch = Clock.System.now().epochSeconds + hourEpoch(1) - 5 }
        weatherTomorrow = Weather().apply { timeEpoch = Clock.System.now().epochSeconds + hourEpoch(2) }
        weatherForecastdays = listOf(
            WeatherForecastday().apply {
                astro = weatherAstronomyToday
                hour = listOf(
                    Weather().apply { timeEpoch = Clock.System.now().epochSeconds - hourEpoch(3) },
                    Weather().apply { timeEpoch = Clock.System.now().epochSeconds - hourEpoch(2) },
                    Weather().apply { timeEpoch = Clock.System.now().epochSeconds - hourEpoch(1) },
                    weatherToday,
                ).toRealmList()
            },
            WeatherForecastday().apply {
                astro = weatherAstronomyTomorrow
                hour = listOf(
                    weatherTomorrow,
                    Weather().apply { timeEpoch = Clock.System.now().epochSeconds + hourEpoch(3) },
                    Weather().apply { timeEpoch = Clock.System.now().epochSeconds + hourEpoch(4) },
                    Weather().apply { timeEpoch = Clock.System.now().epochSeconds + hourEpoch(5) },
                ).toRealmList()
            },
        )
    }

    @Test
    fun `test getFromNetwork should return weather forecast from getUseCase`() = runTest {
        // Arrange
        val weatherForecast = WeatherForecast()
        coEvery { getUseCase.call(query) } returns weatherForecast
        coEvery { setUseCase.call(weatherForecast) } returns weatherForecast

        // Act
        viewModel.getFromNetwork(query)
        advanceUntilIdle()

        // Assert
        assertNotNull(viewModel.item.value)
        assertEquals(weatherForecast, viewModel.item.value)
    }

    @Test
    fun `test getFromLocal should return weather forecast from readUseCase`() = runTest {
        // Arrange
        val weatherForecast = WeatherForecast()
        coEvery { readUseCase.call(query) } returns weatherForecast

        // Act
        viewModel.getFromLocal(query)
        advanceUntilIdle()

        // Assert
        assertNotNull(viewModel.item.value)
        assertEquals(weatherForecast, viewModel.item.value)
    }

    @Test
    fun `test saveToLocal should update weather forecast`() = runTest {
        // Arrange
        viewModel.item.value = WeatherForecast()
        val newWeatherForecast = WeatherForecast().also { it.current = Weather() }
        coEvery { setUseCase.call(newWeatherForecast) } returns newWeatherForecast

        // Act
        viewModel.saveToLocal(newWeatherForecast)
        advanceUntilIdle()

        // Assert
        assertEquals(newWeatherForecast, viewModel.item.value)
    }

    @Test
    fun `test prepareWeathers should return map with closest weather today and all weathers`() {
        // Act
        val result = viewModel.prepareWeathers(weatherForecastdays)

        // Assert
        assertNotNull(result["closest"])
        assertNotNull(result["weathers"])
        assertEquals(weatherToday.timeEpoch, (result["closest"] as Weather).timeEpoch)
        assertEquals(8, (result["weathers"] as List<Weather>).size)
    }

    @Test
    fun `test prepareWeathers should return map with closest weather tomorrow and all weathers`() {
        // Act
        val result = viewModel.prepareWeathers(weatherForecastdays, Clock.System.now().epochSeconds + hourEpoch(2) - 200)

        // Assert
        assertNotNull(result["closest"])
        assertNotNull(result["weathers"])
        assertEquals(weatherTomorrow.timeEpoch, (result["closest"] as Weather).timeEpoch)
        assertEquals(8, (result["weathers"] as List<Weather>).size)
    }

    @Test
    fun `test filterWeathers should return filtered list of weathers today`() {
        // Act
        val result = viewModel.filterWeathers(weatherForecastdays)

        // Assert
        assertEquals(4, result.size)
    }

    @Test
    fun `test filterWeathers should return filtered list of weathers mix today and tomorrow`() {
        // Act
        val result = viewModel.filterWeathers(weatherForecastdays, Clock.System.now().epochSeconds + hourEpoch(2) - 200)

        // Assert
        assertEquals(4, result.size)
    }

    @Test
    fun `test prepareAstronomy should return astronomy based on closest weather today`() {
        // Act
        val result = viewModel.prepareAstronomy(weatherForecastdays)

        // Assert
        assertNotNull(result)
        assertEquals(weatherAstronomyToday.sunrise, result.sunrise)
        assertEquals(weatherAstronomyToday.sunset, result.sunset)
    }

    @Test
    fun `test prepareAstronomy should return astronomy based on closest weather tomorrow`() {
        // Act
        val result = viewModel.prepareAstronomy(weatherForecastdays, Clock.System.now().epochSeconds + hourEpoch(2) - 200)

        // Assert
        assertNotNull(result)
        assertEquals(weatherAstronomyTomorrow.sunrise, result.sunrise)
        assertEquals(weatherAstronomyTomorrow.sunset, result.sunset)
    }
}
