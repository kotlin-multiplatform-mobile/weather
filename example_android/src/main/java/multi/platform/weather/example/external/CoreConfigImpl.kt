package multi.platform.weather.example.external

import multi.platform.core.shared.external.CoreConfig

class CoreConfigImpl(
    override val apiAuthPath: String? = null,
    override val apiChannel: String? = null,
    override val apiRefreshTokenPath: String? = null,
    override val headerChannel: String? = "x-header-channel",
    override val headerDeviceId: String? = "x-header-device-id",
    override val headerLanguage: String? = "x-header-language",
    override val headerOs: String? = "x-header-version",
    override val headerVersion: String? = "x-header-version",
) : CoreConfig
