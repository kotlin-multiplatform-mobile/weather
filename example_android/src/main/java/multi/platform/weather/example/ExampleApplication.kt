package multi.platform.weather.example

import android.provider.Settings
import com.chuckerteam.chucker.api.ChuckerInterceptor
import multi.platform.core.shared.CoreApplication
import multi.platform.core.shared.external.CoreConfig
import multi.platform.core.shared.external.extensions.toMD5
import multi.platform.weather.example.external.CoreConfigImpl
import multi.platform.weather.example.external.WeatherConfigImpl
import multi.platform.weather.shared.WeatherModule
import okhttp3.OkHttpClient
import org.koin.core.KoinApplication
import org.koin.dsl.module
import javax.net.ssl.X509TrustManager

class ExampleApplication : CoreApplication() {
    override fun host() = BuildConfig.WEATHER_SERVER
    override fun sharedPrefsName() = "w34th3R"
    override fun appVersion() = getString(R.string.app_version)

    @Suppress("HardwareIds")
    override fun deviceId() =
        Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
            .toString().toMD5()

    override fun provideOkHttpClient(x509TrustManager: X509TrustManager): OkHttpClient.Builder {
        val chucker = ChuckerInterceptor.Builder(applicationContext).createShortcut(true).build()
        return super.provideOkHttpClient(x509TrustManager).addInterceptor(chucker)
    }

    override val koinApp: (KoinApplication) -> Unit = { app ->
        app.apply {
            modules(
                module {
                    single<CoreConfig> { CoreConfigImpl() }
                },
                WeatherModule(WeatherConfigImpl())(),
            )
        }
    }
}
