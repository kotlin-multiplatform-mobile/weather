package multi.platform.weather.example.external

import multi.platform.weather.example.BuildConfig
import multi.platform.weather.shared.external.WeatherConfig

class WeatherConfigImpl(
    override val host: String = BuildConfig.WEATHER_SERVER,
    override val apiKey: String = BuildConfig.WEATHER_API_KEY,
) : WeatherConfig
