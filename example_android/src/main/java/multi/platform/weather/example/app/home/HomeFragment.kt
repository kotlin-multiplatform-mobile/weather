package multi.platform.weather.example.app.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Lifecycle
import androidx.preference.PreferenceManager
import io.ktor.client.call.body
import io.ktor.client.statement.HttpResponse
import multi.platform.core.shared.app.common.CoreFragment
import multi.platform.core.shared.external.extensions.launchAndCollectIn
import multi.platform.weather.example.R
import multi.platform.weather.example.databinding.HomeFragmentBinding
import multi.platform.weather.shared.app.astronomy.WeatherAstronomyFragment
import multi.platform.weather.shared.app.astronomy.WeatherAstronomyListener
import multi.platform.weather.shared.app.common.ItemWeatherForecastViewModel
import multi.platform.weather.shared.app.currentcondition.WeatherCurrentConditionFragment
import multi.platform.weather.shared.app.currentcondition.WeatherCurrentConditionListener
import multi.platform.weather.shared.app.currenttemp.WeatherCurrentTempFragment
import multi.platform.weather.shared.app.currenttemp.WeatherCurrentTempListener
import multi.platform.weather.shared.app.forecast.WeatherForecastFragment
import multi.platform.weather.shared.app.forecast.WeatherForecastListener
import multi.platform.weather.shared.app.searchbar.WeatherSearchbarFragment
import multi.platform.weather.shared.app.searchbar.WeatherSearchbarListener
import multi.platform.weather.shared.domain.weather.entity.WeatherError
import multi.platform.weather.shared.domain.weather.entity.WeatherForecast
import multi.platform.weather.shared.external.constants.WeatherKey
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : CoreFragment(), WeatherSearchbarListener {
    private val itemWeatherForecastViewModel: ItemWeatherForecastViewModel by viewModel()
    private lateinit var binding: HomeFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.home_fragment, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.weatherForecastVM = itemWeatherForecastViewModel.also {
            it.id = PreferenceManager.getDefaultSharedPreferences(requireContext())
                .getString(WeatherKey.QUERY, "Jakarta")
            it.loadingIndicator.launchAndCollectIn(
                this@HomeFragment,
                Lifecycle.State.STARTED,
            ) { l ->
                onWeatherForecastLoading(l)
                it.loadingIndicator.value = null
            }
            it.item.launchAndCollectIn(this@HomeFragment, Lifecycle.State.STARTED) { i ->
                onWeatherForecastChanged(i)
                it.item.value = null
            }
            it.errorResponse.launchAndCollectIn(this@HomeFragment, Lifecycle.State.STARTED) { r ->
                onWeatherForecastError(r)
                it.errorResponse.value = null
            }
        }
        binding.srlWeather.setOnRefreshListener {
            itemWeatherForecastViewModel.load()
        }
    }

    override fun onResume() {
        super.onResume()
        itemWeatherForecastViewModel.load()
    }

    override fun onWeatherSearch(query: String) {
        PreferenceManager.getDefaultSharedPreferences(requireContext()).edit()
            .putString(WeatherKey.QUERY, query).apply()
        itemWeatherForecastViewModel.id = query
        itemWeatherForecastViewModel.load()
    }

    fun onWeatherForecastLoading(loading: Boolean?) {
        loading?.let { l ->
            binding.loadingView.clLoading.isVisible = l
            binding.loadingView.cpiLoading.isVisible = l
            if (!l) binding.srlWeather.isRefreshing = false
        }
    }

    fun onWeatherForecastChanged(weatherForecast: WeatherForecast?) {
        weatherForecast?.let { i ->
            var nightMode = AppCompatDelegate.MODE_NIGHT_YES
            if (i.current?.isDay == 1) nightMode = AppCompatDelegate.MODE_NIGHT_NO
            if (AppCompatDelegate.getDefaultNightMode() != nightMode) {
                PreferenceManager.getDefaultSharedPreferences(requireContext()).edit()
                    .putInt(WeatherKey.NIGHT_MODE, nightMode).apply()
                requireActivity().finish()
                requireActivity().startActivity(requireActivity().intent)
            }
            binding.clWeatherCurrent.isVisible = true
            binding.fcvWeatherAstronomy.isVisible = true
            binding.fcvWeatherForecast.isVisible = true

            PreferenceManager.getDefaultSharedPreferences(requireContext()).edit()
                .putString(WeatherKey.QUERY, i.location?.name.toString()).apply()
            (binding.fcvWeatherSearchbar.getFragment<WeatherSearchbarFragment>() as WeatherSearchbarListener)
                .refreshWeatherSearch()
            i.current?.tempC?.let { temp ->
                (binding.fcvWeatherCurrentTemp.getFragment<WeatherCurrentTempFragment>() as WeatherCurrentTempListener)
                    .onDataChanged(
                        i.location?.localtime.toString(),
                        i.location?.name.toString(),
                        temp,
                    )
            }
            i.current?.condition?.let { con ->
                (binding.fcvWeatherCurrentCondition.getFragment<WeatherCurrentConditionFragment>() as WeatherCurrentConditionListener)
                    .onDataChanged(con)
            }
            i.forecast?.forecastday?.let { f ->
                itemWeatherForecastViewModel.prepareAstronomy(f)?.let { a ->
                    (binding.fcvWeatherAstronomy.getFragment<WeatherAstronomyFragment>() as WeatherAstronomyListener)
                        .onDataChanged(a)
                }
                (binding.fcvWeatherForecast.getFragment<WeatherForecastFragment>() as WeatherForecastListener)
                    .onDataChanged(itemWeatherForecastViewModel.filterWeathers(f))
            }
        }
    }

    suspend fun onWeatherForecastError(response: HttpResponse?) {
        response?.let { r ->
            val weatherError: WeatherError = r.body()
            binding.tvError.text = weatherError.error.message

            binding.clWeatherCurrent.isVisible = false
            binding.fcvWeatherAstronomy.isVisible = false
            binding.fcvWeatherForecast.isVisible = false
            binding.tvError.isVisible = true
        }
    }
}
