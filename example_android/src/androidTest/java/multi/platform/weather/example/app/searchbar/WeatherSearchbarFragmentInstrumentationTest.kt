package multi.platform.weather.example.app.searchbar

import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.preference.PreferenceManager
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.clearText
import androidx.test.espresso.action.ViewActions.closeSoftKeyboard
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import multi.platform.weather.example.R
import multi.platform.weather.shared.app.searchbar.WeatherSearchbarFragment
import multi.platform.weather.shared.external.constants.WeatherKey
import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters
import multi.platform.weather.shared.R as wR

@RunWith(AndroidJUnit4::class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class WeatherSearchbarFragmentInstrumentationTest {

    private lateinit var scenario: FragmentScenario<WeatherSearchbarFragment>
    private val initialSearchQuery = "Bandung"

    @Before
    fun setup() {
        scenario = launchFragmentInContainer<WeatherSearchbarFragment>(null, R.style.AppTheme)
    }

    @Test
    fun testWeatherSearchbarFragment() {
        scenario.onFragment { fragment ->
            PreferenceManager.getDefaultSharedPreferences(fragment.requireContext())
                .edit().remove(WeatherKey.QUERY).apply()
        }
        // Verify the initial state of the WeatherSearchbarFragment
        onView(withId(wR.id.act_search_location)).check(matches(isDisplayed()))

        // Perform a text input action
        onView(withId(wR.id.act_search_location)).perform(clearText(), typeText(initialSearchQuery), closeSoftKeyboard())

        // Verify the entered text
        onView(withId(wR.id.act_search_location)).check(matches(withText(initialSearchQuery)))
    }

    @Test
    fun testWeatherSearchbarFragmentRefreshWeatherSearch() {
        // Verify the expected behavior after the click action
        onView(withId(wR.id.act_search_location)).check(matches(withText(initialSearchQuery)))
    }
}
