@file:Suppress("UnstableApiUsage", "DataBindingWithoutKapt")

import com.android.build.gradle.internal.cxx.configure.gradleLocalProperties
import org.jetbrains.kotlin.util.capitalizeDecapitalize.toUpperCaseAsciiOnly

plugins {
    alias(libs.plugins.androidApplication)
    alias(libs.plugins.kotlinAndroid)
    id("androidx.navigation.safeargs.kotlin")
    id("kotlinx-serialization")
}

val repoId: String by project
val appId: String by project
val appName: String by project
val appVersionName: String by project
val appVersionCode: String by project
val androidCompileSdkVersion: String by project
val androidTargetSdkVersion: String by project
val androidMinSdkVersion: String by project
val iosDeploymentTarget: String by project
val localProperties = gradleLocalProperties(rootDir)

android {
    namespace = "$appId.example"
    compileSdk = androidCompileSdkVersion.toInt()
    defaultConfig {
        applicationId = "$appId.example"
        minSdk = androidMinSdkVersion.toInt()
        targetSdk = androidTargetSdkVersion.toInt()
        versionCode = appVersionCode.toInt()
        versionName = appVersionName

        vectorDrawables.useSupportLibrary = true
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = true
            setProguardFiles(mutableListOf("proguard-rules.pro"))
        }
        getByName("debug") {
            isMinifyEnabled = false
            enableUnitTestCoverage = true
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }

    buildFeatures {
        buildConfig = true
        dataBinding = true
    }

    flavorDimensions += "api"
    productFlavors {
        val mainFlavour = localProperties.getProperty("mainFlavor")
        val flavours = localProperties.getProperty("flavors").split(",")
        flavours.forEach {
            create(it) {
                dimension = "api"
                applicationIdSuffix = if (it != mainFlavour) ".${it}" else ""
                versionNameSuffix = if (it != mainFlavour) "-${it}" else ""
                resValue(
                    "string",
                    "app_name",
                    "Weather${if (it != mainFlavour) " (${it.toUpperCaseAsciiOnly()})" else ""}"
                )
                resValue(
                    "string",
                    "app_version",
                    String.format("%s%s", "${defaultConfig.versionName}", "${versionNameSuffix}")
                )
                resValue(
                    "string", "route_home_full",
                    localProperties.getProperty("scheme") + (if (it != mainFlavour) "-$it" else "") + "://" +
                            localProperties.getProperty("host") + "/home"
                )
                resValue(
                    "string",
                    "route_home",
                    localProperties.getProperty("host") + "/home"
                )
            }
        }
    }
}

dependencies {
    implementation(project(":weather_shared"))
    implementation(libs.fragment.testing)
    implementation(libs.junit.ktx)
    implementation(libs.espresso.core)
    implementation(libs.coroutines.test)
    testImplementation(libs.junit)
    debugImplementation(libs.chucker)
    releaseImplementation(libs.chucker.no.op)
}